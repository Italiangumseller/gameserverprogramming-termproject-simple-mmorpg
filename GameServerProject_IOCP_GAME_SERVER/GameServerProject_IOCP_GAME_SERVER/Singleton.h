#pragma once
template<class T>
class Singleton {
public:
	Singleton() {};
	virtual ~Singleton() {};

	static T* get() {
		if (m_instance == nullptr)
			m_instance = new T;
		return m_instance;
	}

private:
	static T* m_instance;
};
template<class T> T* Singleton<T>::m_instance = nullptr;