#pragma once
#include "Object.h"
class Knight final :  public Object
{
public:
	Knight();
	Knight(const int& _id, const wstring& _name, const char& _objType, const unsigned short& _hp, const unsigned short& _maxHp,
		const unsigned char& _level, const unsigned int& _curExp, const unsigned int& _maxExp, const unsigned short& _atk,
		const Coord& _pos);
	Knight(const Knight& _other) noexcept;
	Knight& operator=(const Knight& _other) noexcept;
	virtual ~Knight();

public:
	void IncreaseExp(const unsigned short& _exp);
	void DecreaseExp();
	void LevelUp();
	void TelePort(const Coord& _pos);

	virtual void Move(const Map& _map, const char& _dir);
	virtual void Attack(Object* _target) override;
	//virtual void Damaged(const unsigned short& _damage, bool& _isDead);
	virtual void Die() override;
	virtual void Revive() override;

private:
};

