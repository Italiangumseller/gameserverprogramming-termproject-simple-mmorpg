#pragma once

constexpr size_t SECTOR_WIDTH = 10;
constexpr size_t SECTOR_HEIGHT = 10;
constexpr size_t SECTOR_NUM_ROW = 800 / SECTOR_WIDTH + 1;
constexpr size_t SECTOR_NUM_COL = 800 / SECTOR_HEIGHT + 1;

constexpr size_t NUM_WORKERTHREADS = 4;
constexpr size_t MAX_BUFFER = 255;
constexpr int ACCEPT_KEY = -100;

constexpr size_t ID_LEN = 10;
constexpr short VIEW_RANGE = 3;
constexpr short NPC_VIEW_RANGE = 4;


enum EVENT_TYPE {
	EV_RECV, EV_SEND, EV_ACCEPT,
	// PC EVENT TYPES
	EV_MOVE, EV_ATTACK, EV_HEAL, EV_DIE, EV_RESPAWN, EV_SAVE_PLAYER_INFO,
	// NPC EVENT TYPES
	EV_PLAYER_MOVE_NOTIFY, EV_MOVE_NPC, EV_ATTACKED_NPC,
	EV_PEACE_NPC, EV_DIE_MONSTER, EV_RESPAWN_MONSTER, EV_ATTACK_NPC
};

constexpr unsigned short MAX_LEVEL = 60;

typedef struct position {
	short x, y;
} POS;

typedef struct player_info {
	wchar_t					id_str[10];
	unsigned short			hp;
	unsigned short			maxHp;
	unsigned char			lvl;
	unsigned int			curExp;
	unsigned short			atk;
	short					x, y;
}P_INFO;

typedef struct monster_info {
	char		id_str[20];
	unsigned char		level;
	short		monster_type;		// 평화, 전쟁
	unsigned int			exp;				// 사냥시 플레이어가 얻는 경험치
	unsigned short		hp;					// 몬스터 체력
	unsigned short		atk;				// 몬스터 공격력
	short		x, y;				// 몬스터 위치
}M_INFO;

struct OVER_EX {
	WSAOVERLAPPED over;
	union {
		WSABUF	wsabuf[1];
		SOCKET	socket;
	};
	char	net_buf[MAX_BUFFER];
	EVENT_TYPE	ev_type;
};

class Object;
struct SOCKETINFO
{
	OVER_EX			recv_over;
	SOCKET			socket;
	int				id;
	bool			is_connected;

	char			packet_buffer[MAX_BUFFER];
	unsigned int	saved_packet_size;
	unsigned int	curr_packet_size;

	Object*			obj;
	unordered_set <int>		near_id;
	pair<short, short>	mySector;
	shared_mutex			near_lock;
	lua_State* L;
};

// {obj_id, wakeup_time, ev_type, target_obj}
struct EVENT {
	int obj_id; // 어떤 오브젝트에서 이벤트를 시작해야하는가?
	high_resolution_clock::time_point wakeup_time; // 언제시작?
	int event_type; // 무슨 이벤트인가?
	int target_obj; // 목표가 있어서 추격한다면 필요하다.
	constexpr bool operator<(const EVENT& left) const // ref로 복사 안일어나게
	{
		return wakeup_time > left.wakeup_time;
	}
};

