#include "stdafx.h"
#include "DB_Mgr.h"

DB_Mgr::DB_Mgr()
{
	Initilize();
}

DB_Mgr::~DB_Mgr()
{
	// Process data  
	if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
		SQLCancel(hstmt); // 더 이상 안쓸것들 처리....
		SQLFreeHandle(SQL_HANDLE_STMT, hstmt);
	}
	SQLDisconnect(hdbc);
	SQLFreeHandle(SQL_HANDLE_DBC, hdbc);
	SQLFreeHandle(SQL_HANDLE_ENV, henv);
	//system( "pause" );

}

void DB_Mgr::Initilize()
{
	setlocale(LC_ALL, "korean");

	// Allocate environment handle  
	retcode = SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &henv);

	// Set the ODBC version environment attribute  
	if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
		retcode = SQLSetEnvAttr(henv, SQL_ATTR_ODBC_VERSION, (SQLPOINTER*)SQL_OV_ODBC3, 0);

		// Allocate connection handle  
		if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
			retcode = SQLAllocHandle(SQL_HANDLE_DBC, henv, &hdbc);

			// Set login timeout to 5 seconds  
			if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
				SQLSetConnectAttr(hdbc, SQL_LOGIN_TIMEOUT, (SQLPOINTER)5, 0);

				// Connect to data source  
				retcode = SQLConnect(hdbc, (SQLWCHAR*)L"2014182019_박태준", SQL_NTS, (SQLWCHAR*)NULL, SQL_NTS, NULL, SQL_NTS);
				if (retcode == SQL_SUCCESS_WITH_INFO)
					std::cout << "DB connected!" << std::endl;
				else
					std::cout << "DB connect failed!" << std::endl;
			}
		}
	}
}

void DB_Mgr::show_error()
{
	printf("error\n");

}
void DB_Mgr::HandleDiagnosticRecord(SQLHANDLE hHandle, SQLSMALLINT hType, RETCODE RetCode)
{
	SQLSMALLINT iRec = 0;
	SQLINTEGER  iError;
	WCHAR       wszMessage[1000];
	WCHAR       wszState[SQL_SQLSTATE_SIZE + 1];
	if (RetCode == SQL_INVALID_HANDLE) {
		fwprintf(stderr, L"Invalid handle!\n");
		return;
	}
	while (SQLGetDiagRec(hType, hHandle, ++iRec, wszState, &iError, wszMessage, (SQLSMALLINT)(sizeof(wszMessage) / sizeof(WCHAR)), (SQLSMALLINT*)NULL) == SQL_SUCCESS)
	{ // Hide data truncated.. 
		if (wcsncmp(wszState, L"01004", 5)) {
			fwprintf(stderr, L"[%5.5s] %s (%d)\n", wszState, wszMessage, iError);
		}
	}
}

P_INFO DB_Mgr::GetUserInfo(const wstring& id_str)
{
	std::wstring stored_proceser = L"EXEC GetUserInfo ";
	stored_proceser += id_str;
	if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
		retcode = SQLAllocHandle(SQL_HANDLE_STMT, hdbc, &hstmt);

		retcode = SQLExecDirect(hstmt, (SQLWCHAR*)stored_proceser.c_str(), SQL_NTS);
		if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
			retcode = SQLBindCol(hstmt, 1, SQL_UNICODE_CHAR, &szID, ID_LEN * 2, &cbID);
			retcode = SQLBindCol(hstmt, 2, SQL_INTEGER, &nHP, 10, &cbHP);
			retcode = SQLBindCol(hstmt, 3, SQL_INTEGER, &nMaxHp, 10, &cbMaxHp);
			retcode = SQLBindCol(hstmt, 4, SQL_INTEGER, &nLV, 10, &cbLV);
			retcode = SQLBindCol(hstmt, 5, SQL_INTEGER, &nATK, 10, &cbATK);
			retcode = SQLBindCol(hstmt, 6, SQL_INTEGER, &nEXP, 10, &cbEXP);
			retcode = SQLBindCol(hstmt, 7, SQL_INTEGER, &nPosX, 10, &cbPosX);
			retcode = SQLBindCol(hstmt, 8, SQL_INTEGER, &nPosY, 10, &cbPosY);

			retcode = SQLFetch(hstmt); // SQLFetch로 받는다. 
			// 에러 OR 성공 OR 다 읽음.
			if (retcode == SQL_ERROR)
				show_error();
			if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
				P_INFO player_info;

				player_info.hp = nHP;
				player_info.maxHp = nMaxHp;
				player_info.curExp = nEXP;
				player_info.lvl = nLV;
				player_info.atk = nATK;
				player_info.x = nPosX;
				player_info.y = nPosY;

				int size{};
				for (int i = 0; i < ID_LEN; ++i) {
					if (szID[i] == ' ') {
						szID[i] = '\0';
					}
				}

				wmemcpy(player_info.id_str, szID, ID_LEN);

				//std::wcout << "넘어온 ID - " << player_info.id_str << std::endl;
				SQLFreeHandle(SQL_HANDLE_STMT, hstmt);
				return player_info;
			}
			else {
				HandleDiagnosticRecord(hstmt, SQL_HANDLE_STMT, retcode);
				std::cout << "존재하지 않는 ID이므로 캐릭터를 새로 생성해야 합니다." << std::endl;
			}
		}
		else
			HandleDiagnosticRecord(hstmt, SQL_HANDLE_STMT, retcode);

		SQLFreeHandle(SQL_HANDLE_STMT, hstmt);
		return P_INFO{ NULL };
	}
}

P_INFO DB_Mgr::AddUserInfo(std::wstring id_str)
{
	std::wstring stored_proceser = L"EXEC AddUserInfo ";

	stored_proceser += id_str;
	retcode = SQLAllocHandle(SQL_HANDLE_STMT, hdbc, &hstmt);

	retcode = SQLExecDirect(hstmt, (SQLWCHAR*)stored_proceser.c_str(), SQL_NTS);
	if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
		std::wcout << id_str << L"로 새로운 캐릭터를 생성했습니다." << std::endl;

		if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
			retcode = SQLBindCol(hstmt, 1, SQL_C_WCHAR, &szID, 10, &cbID);
			retcode = SQLBindCol(hstmt, 2, SQL_INTEGER, &nHP, 10, &cbHP);
			retcode = SQLBindCol(hstmt, 3, SQL_INTEGER, &nMaxHp, 10, &cbMaxHp);
			retcode = SQLBindCol(hstmt, 4, SQL_INTEGER, &nLV, 10, &cbLV);
			retcode = SQLBindCol(hstmt, 5, SQL_INTEGER, &nATK, 10, &cbATK);
			retcode = SQLBindCol(hstmt, 6, SQL_INTEGER, &nEXP, 10, &cbEXP);
			retcode = SQLBindCol(hstmt, 7, SQL_INTEGER, &nPosX, 10, &cbPosX);
			retcode = SQLBindCol(hstmt, 8, SQL_INTEGER, &nPosY, 10, &cbPosY);

			retcode = SQLFetch(hstmt); // SQLFetch로 받는다. 
			// 에러 OR 성공 OR 다 읽음.
			if (retcode == SQL_ERROR)
				show_error();
			if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
				P_INFO player_info;

				player_info.hp = nHP;
				player_info.maxHp = nMaxHp;
				player_info.curExp = nEXP;
				player_info.lvl = nLV;
				player_info.atk = nATK;
				player_info.x = nPosX;
				player_info.y = nPosY;
				wmemcpy(player_info.id_str, szID, ID_LEN);

				SQLFreeHandle(SQL_HANDLE_STMT, hstmt);
				return player_info;
			}
			else HandleDiagnosticRecord(hstmt, SQL_HANDLE_STMT, retcode);
		}
		else
			HandleDiagnosticRecord(hstmt, SQL_HANDLE_STMT, retcode);

		SQLFreeHandle(SQL_HANDLE_STMT, hstmt);
		return P_INFO{ NULL };
	}
}

void DB_Mgr::SetUserInfo(P_INFO info)
{
	std::wstring stored_proceser = L"EXEC SetUserInfo ";
	std::wstring posX, posY, HP, MAXHP, LV, ATK, EXP, ID;
	posX = std::to_wstring(info.x);
	posY = std::to_wstring(info.y);
	HP = std::to_wstring(info.hp);
	MAXHP = std::to_wstring(info.maxHp);
	LV = std::to_wstring(info.lvl);
	ATK = std::to_wstring(info.atk);
	EXP = std::to_wstring(info.curExp);
	ID = info.id_str;

	stored_proceser += ID + L"," + HP + L"," + MAXHP + L"," + LV + L"," + ATK + L"," + EXP + L"," + posX + L"," + posY;
	if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
		retcode = SQLAllocHandle(SQL_HANDLE_STMT, hdbc, &hstmt);

		retcode = SQLExecDirect(hstmt, (SQLWCHAR*)stored_proceser.c_str(), SQL_NTS);
		if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) {
			std::cout << "종료한 플레이어의 위치를 DB에 갱신했습니다." << std::endl;
		}
		else
			HandleDiagnosticRecord(hstmt, SQL_HANDLE_STMT, retcode);
	}
}

bool DB_Mgr::SetPos(const wstring& id_str, const short& posX, const short& posY)
{
	std::wstring stored_procedure = L"EXEC SetPos ";
	stored_procedure += id_str + L"," + std::to_wstring(posX) + L"," + std::to_wstring(posY);

	retcode = SQLExecDirect(hstmt, (SQLWCHAR*)stored_procedure.c_str(), SQL_NTS);
	if (retcode == SQL_SUCCESS || retcode == SQL_SUCCESS_WITH_INFO) return true;
	else HandleDiagnosticRecord(hstmt, SQL_HANDLE_STMT, retcode);
	return false;
}

