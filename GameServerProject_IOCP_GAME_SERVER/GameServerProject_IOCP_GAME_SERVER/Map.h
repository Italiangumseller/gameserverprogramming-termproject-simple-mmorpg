#pragma once
#include <iostream>
#include <fstream>
using namespace std;

struct Tile {
	char tile;
	char flag{0};
};

class Map {
public:
	Map() {
		w = h = 800;
		cout << "Creating GameWorld!" << endl;

		ifstream in;
		in.open("GameWorld.txt");
		for (int r = 0; r < h; ++r)
			for (int s = 0; s < w; ++s) {
				short tile;
				in >> tile;
				m[r][s].tile = tile;
			}

		in.close();
		cout << "GameWorld has been completed!" << endl;
	}
	int operator() (const short& x, const short& y) { return m[x][y].tile; }
	bool isValid(const short& x, const short& y)  const {
		return (m[x][y].tile != 4 && m[x][y].flag == 0 && x >= 0 && x < WORLD_WIDTH && y >= 0 && y < WORLD_HEIGHT);
	}
	Tile m[800][800];
	int w, h;
};