#pragma once
#include "Coord.h"
#include "Global.h"
#include "Server.h"

class Map;
class Object
{
public:
	Object();
	Object(const int& _id, const wstring& _name, const char& _objType, const unsigned short& _hp, const unsigned short& _maxHp,
		const unsigned char& _level, const unsigned int& _curExp, const unsigned int& _maxExp, const unsigned short& _atk,
		const Coord& _pos);

	Object(const Object& _other) noexcept;
	Object& operator=(const Object& _other) noexcept;

	Object(Object&& _other) noexcept;
	Object& operator=(Object&& _other) noexcept;

	virtual ~Object();


public:
	inline void SetName(const wstring& _name)				{  m_name = _name; }
	inline void SetObjType(const char& _objType)			{  m_objType = _objType; }
	inline void SetHp(const unsigned short& _hp)			{  m_hp = _hp; }
	inline void SetMaxHp(const unsigned short& _maxHp)		{  m_maxHp = _maxHp; }
	inline void SetLevel(const unsigned short& _level)		{  m_level = _level; }
	inline void SetCurExp(const unsigned int& _curExp )		{  m_curExp = _curExp; }
	inline void SetMaxExp(const unsigned int& _maxExp )		{  m_maxExp = _maxExp; }
	inline void SetAtk(const unsigned short& _atk )			{  m_atk = _atk; }
	inline void SetPos(const Coord& _pos )					{  m_pos = _pos; }
	inline void SetAlive(const bool& _isAlive)				{ m_isAlive = _isAlive; }
	inline void SetActive(const bool& _isActive)			{ m_isActive = _isActive; }

	inline int GetId() const								{ return m_id; }
	inline wstring GetName() const							{ return m_name; }
	inline char GetObjType() const							{ return m_objType; }
	inline unsigned short GetHp() const						{ return m_hp; }
	inline unsigned short GetMaxHp() const					{ return m_maxHp; }
	inline unsigned char GetLevel() const					{ return m_level; }
	inline unsigned int GetCurExp() const					{ return m_curExp; }
	inline unsigned int GetMaxExp() const					{ return m_maxExp; }
	inline unsigned short GetAtk() const					{ return m_atk; }
	inline Coord GetPos() const								{ return m_pos; }
	inline bool IsAlive() const								{ return m_isAlive; }
	inline bool IsActive() const							{ return m_isActive; }

public:
	virtual void Move(const Map& _map, const char& _dir) {}
	virtual void Damaged(const int& _attacker, const unsigned short& _damage, bool& _isDead, const EVENT_TYPE& _evType);
	virtual bool IsInRange(const Coord& pos) {
		return m_pos.IsNeighbor(pos);
	}

	virtual void Attack(Object* _obj) {};
	virtual void Die() {
		m_hp = 0;

	};
	virtual void Revive() {};
	virtual void Heal() {
		m_hp = m_maxHp;
	}

protected:
	int					m_id;
	wstring				m_name;
	char				m_objType;
	unsigned short		m_hp;
	unsigned short		m_maxHp;
	unsigned char		m_level;
	unsigned int		m_curExp;
	unsigned int		m_maxExp;
	unsigned short		m_atk;
	Coord				m_pos;

	atomic_bool			m_isAlive;
	bool				m_isActive;
};

