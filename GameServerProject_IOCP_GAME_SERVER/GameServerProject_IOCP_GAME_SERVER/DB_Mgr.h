#pragma once
#include <sqlext.h>
#include "Singleton.h"
#include "Global.h"


class DB_Mgr
{
public:
	DB_Mgr();
	~DB_Mgr();
	void Initilize();

	void show_error();

	void HandleDiagnosticRecord( SQLHANDLE hHandle, SQLSMALLINT hType, RETCODE RetCode );

	// DB - Stored Proceser 호출
	// ID를 확인하고 DB에 있는 ID면 포지션 x, y 반환하는 함수
	POS GetPos( std::wstring ID);
	
	// ID를 확인하고 있으면, 플레이어 정보(ID, HP, EXP, POSX, POSY)를 꺼내오는 함수
	P_INFO GetUserInfo( const wstring& id_str );
	P_INFO AddUserInfo( std::wstring id_str );
	
	// 클라이언트가 접속 종료하면 마지막 위치를 DB에 UPDATE하는 함수.
	void SetUserInfo( P_INFO info );
	bool SetPos(const wstring& id_str, const short& posX, const short& posY);

private:
	SQLHENV henv;
	SQLHDBC hdbc;
	SQLHSTMT hstmt = 0;
	SQLRETURN retcode;
	SQLINTEGER nPosX, nPosY, nHP, nMaxHp, nATK, nLV, nEXP;
	SQLWCHAR szID[ID_LEN];
	SQLLEN cbID = 0, cbPosX = 0, cbPosY = 0, cbHP = 0, cbMaxHp = 0, cbATK = 0, cbLV = 0, cbEXP = 0;
};

