#include "stdafx.h"
#include "NPC.h"
#include "Astar.h"

void NPC::Move(Map* _map, const Coord& _targetPos, const int& _targetId)
{
	// 직선상으로 이동 가능하면 해당 방향으로 이동한다.
	// 장애물을 만나거나 방향 전환이 필요하면 A*를 이용해 다음 경로를 찾는다.

	// 이웃 타일에서 타겟 위치와의 거리를 비교함. 제일 가까운 순서를 우선으로 이동( 장애물, 다른 객체가 있으면 다음 우선 순위로 이동? )
	// 가만히 있는게 맞을 수도 있을듯?

	Coord pos{ m_pos }, dstPos{ _targetPos };
	aStar as;
	bool pathFound{ false };
	if (m_id == _targetId) {
		dstPos = m_initialPos;
		if (m_pos.IsNeighbor(dstPos)) {
			m_pos = dstPos;
			return;
		}
	}

	/*Coord neighborTiles[4]{ {pos.x - 1,pos.y}, {pos.x + 1, pos.y}, {pos.x, pos.y - 1}, {pos.x, pos.y + 1} };

	set<pair<int, pair<short, short>>> distanceNeighborTile{};
	for (int i = 0; i < 4; ++i) {
		distanceNeighborTile.insert(make_pair(neighborTiles[i].GetDistance(dstPos), make_pair(neighborTiles[i].x, neighborTiles[i].y)));
	}

	for (const auto& pos : distanceNeighborTile) {
		Coord p{ pos.second.first, pos.second.second };
		if (_map->isValid(p.x, p.y)) {
			m_pos = p;
			break;
		}
	}*/


	// 타일을 우선순위별로 정렬.

	/*if (pos.x == dstPos.x) {
		if (pos.y < dstPos.y)
			pos.y++;
		else pos.y--;

		if (_map->isValid(pos.x, pos.y)) {
			m_pos.y = pos.y;
			pathFound = true;
		}
	}
	else if (pos.y == dstPos.y) {
		if (pos.x < dstPos.x)
			pos.x++;
		else pos.x--;

		if (_map->isValid(pos.x, pos.y)) {
			m_pos.x = pos.x;
			pathFound = true;
		}
	}*/

	if(!pathFound) {
		// 직선 경로를 찾지 못했을때만 호출해보자
		if (as.search(m_pos, dstPos, _map)) {
			std::list<Coord> path;
			as.path(path);

			std::list<Coord>::iterator i = path.begin();
			++i;
			pos.x = (*i).x;
			pos.y = (*i).y;
			m_pos.x = pos.x;
			m_pos.y = pos.y;
		}
	}


	// 타켓이 있으면 추적해서 공격
	// 타겟을 잃으면 초기 위치로 이동해야함.
	if (m_pos.IsNeighbor(dstPos) && m_id != _targetId) {
		EVENT ev{ m_id, high_resolution_clock::now() + 1s, EV_ATTACK_NPC, _targetId };
		Server::get()->add_timer(ev);
		return;
	}
	else {
		EVENT ev{ m_id, high_resolution_clock::now() + 1s, EV_MOVE_NPC, _targetId };
		Server::get()->add_timer(ev);
	}
}

void NPC::Attack(Object* _target)
{
	bool isDead{ false };
	_target->Damaged(m_id, m_atk, isDead, EVENT_TYPE::EV_DIE);

	EVENT ev{};
	if (isDead) {
		// 타겟 없음 상태로 전환, 원래 위치로 이동
		ev = { m_id, high_resolution_clock::now() + 1s, EV_PEACE_NPC, _target->GetId() };
		// 힐 이벤트
	}
	else {
		// 공격 범위에 있으면, 다시 공격
		if (m_pos.IsNeighbor(_target->GetPos())) {
			ev = { m_id, high_resolution_clock::now() + 1s, EV_ATTACK_NPC, _target->GetId() };
		}
		// 공격 범위에 없으면, 추적.
		else
			ev = { m_id, high_resolution_clock::now() + 1s, EV_MOVE_NPC, _target->GetId() };
	}

	Server::get()->add_timer(ev);
}

void NPC::Die()
{
	// 생존 여부를 판단할 변수가 필요. 상태는 객체가 가지고 있는게 맞는듯하다.
	EVENT ev{ m_id, high_resolution_clock::now() + 30s, EV_RESPAWN_MONSTER, NULL };
	Server::get()->add_timer(ev);
}

void NPC::Revive()
{
	m_hp = m_maxHp;
	m_pos = m_initialPos;
}
