//#define _WINSOCK_DEPRECATED_NO_WARNIGS
#pragma comment(lib, "Ws2_32.lib")
#include <iostream>

#include <map>
#include <set>
#include <thread>
#include <mutex>

#include <WS2tcpip.h>
#include <Windows.h>
#include <sqlext.h>

#include "protocol.h"
#include "DB_Mgr.h"

using namespace std;

#define BUFSIZE 512
constexpr auto VIEW_RANGE = 4;

struct OVER_EX{
	WSAOVERLAPPED over;
	WSABUF wsabuf[1];
	char buffer[BUFSIZE];
	bool is_recv;
};

struct SOCKETINFO{
	OVER_EX recv_over;
	SOCKET socket;
	int id;
	short x, y;
	set<int> near_id;
	mutex near_lock;
	wstring user_name;
};

void err_quit( const char* msg )
{
	WCHAR* lpMsgBuf;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
		NULL, WSAGetLastError(),
		MAKELANGID( SUBLANG_NEUTRAL, SUBLANG_DEFAULT ),
		(LPTSTR)&lpMsgBuf, 0, NULL );
	MessageBox( NULL, (LPTSTR)lpMsgBuf, (LPCWSTR)msg, MB_ICONERROR );
	LocalFree( lpMsgBuf );
	exit( 1 );
}
void err_display( const char* msg )
{
	LPVOID lpMsgBuf;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
		NULL, WSAGetLastError(),
		MAKELANGID( LANG_NEUTRAL, SUBLANG_DEFAULT ),
		(LPTSTR)&lpMsgBuf, 0, NULL );
	cout << "[" << msg << "] " << (char*)lpMsgBuf << endl;
	LocalFree( lpMsgBuf );
}

void do_worker();
void ProcessPacket( int id, void* buf );
void send_packet( int id, void* buf );
void send_login_ok_packet( int id );
void send_login_not_ok_packet( int id );
void send_put_player_packet( int client, int new_id );
void send_remove_player_packet( int client, int leaver );
void send_pos_packet( int client, int mover );
void send_connect_result( int id, void* buf );
bool is_near( int a, int b );
bool is_near_id( int player, int other );

void Create_NPC();

void do_ai();

map<SOCKET, SOCKETINFO*> clients;
HANDLE g_iocp;
DB_Mgr db_mgr{};
int g_user_id{};

bool Is_NPC( int id );


int main( int argc, char* argv[] )
{
	wcout.imbue( locale( "korean" ) );


	// Network 만들기 전에 AI 먼저 만들기
	//Create_NPC();




	// 윈속 초기화
	WSADATA wsa;
	if( WSAStartup( MAKEWORD( 2, 2 ), &wsa ) != 0 ) return 1;

	// socket()
	SOCKET listen_sock = WSASocket( AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, 0, WSA_FLAG_OVERLAPPED );
	if( listen_sock == INVALID_SOCKET ) err_quit( "socket() - 대기소켓" );

	// bind()
	SOCKADDR_IN serveraddr;
	ZeroMemory( &serveraddr, sizeof( serveraddr ) );
	serveraddr.sin_addr.s_addr = htonl( INADDR_ANY );
	serveraddr.sin_port = htons( SERVER_PORT );
	serveraddr.sin_family = AF_INET;
	int retval = ::bind( listen_sock, (SOCKADDR*)&serveraddr, sizeof( serveraddr ) );
	if( retval == SOCKET_ERROR ) err_quit( "bind()" );

	// listen()
	retval = listen( listen_sock, SOMAXCONN );
	if( retval == SOCKET_ERROR ) err_quit( "listen()" );

	SOCKET clientSock;
	SOCKADDR_IN clientAddr;
	int addrlen = sizeof( SOCKADDR_IN );
	DWORD flags;

	g_iocp = CreateIoCompletionPort( INVALID_HANDLE_VALUE, NULL, NULL, 0 );
	thread worker_thread0{ do_worker };
	thread worker_thread1{ do_worker };
	thread AI_thread{ do_ai };

	while( true )
	{
		// accept()
		clientSock = accept( listen_sock, (SOCKADDR*)&clientAddr, &addrlen );
		if( clientSock == INVALID_SOCKET ){
			err_display( "accept()" );
			break;
		}

		int user_id = g_user_id++;

		clients[user_id] = new SOCKETINFO;
		clients[user_id]->socket = clientSock;
		clients[user_id]->recv_over.wsabuf[0].len = BUFSIZE;
		clients[user_id]->recv_over.wsabuf[0].buf = clients[user_id]->recv_over.buffer;
		clients[user_id]->recv_over.is_recv = true;
		flags = 0;

		send_login_ok_packet( user_id );
		clients[user_id]->x = 4;
		clients[user_id]->y = 4;

		for( auto& cl : clients ){
			int other_player = cl.first;
			if( true == is_near( other_player, user_id ) ){
				if( Is_NPC( other_player ) == false )
					send_put_player_packet( other_player, user_id );	// npc 아닐때만 보내라.
				if( other_player != user_id )
					send_put_player_packet( user_id, other_player );
			}
		}
		CreateIoCompletionPort( reinterpret_cast<HANDLE>(clientSock), g_iocp, user_id, 0 );
		printf( "\n[TCP Server] 클라이언트 접속: IP addr: %s, Port: %d",
			inet_ntoa( clientAddr.sin_addr ), ntohs( clientAddr.sin_port ) );

		memset( &clients[user_id]->recv_over.over, 0, sizeof( WSAOVERLAPPED ) );
		int retval = WSARecv( clientSock, clients[user_id]->recv_over.wsabuf, 1, NULL,
			&flags, &(clients[user_id]->recv_over.over), NULL );
		if( retval == SOCKET_ERROR ) err_display( "WSARecv()(in main) occur Error! " );
	}
	worker_thread0.join();
	worker_thread1.join();
	AI_thread.join();
	closesocket( listen_sock );
	WSACleanup();
}

void do_worker()
{
	while( true ) {
		DWORD num_byte;
		ULONG key;
		PULONG p_key = &key;
		WSAOVERLAPPED* p_over;

		int retval = GetQueuedCompletionStatus( g_iocp, &num_byte, (PULONG_PTR)p_key, &p_over, INFINITE );
		// retval에 에러 코드받아서 처리해야 깔끔해짐

		SOCKET client_s = clients[key]->socket;

		if( num_byte == 0 ) {
			db_mgr.SetPos( clients[key]->user_name, clients[key]->x, clients[key]->y );
			closesocket( client_s );
			clients.erase( key );
			for( auto& cl : clients )
				send_remove_player_packet( cl.first, key );
			continue;
		}  // 클라이언트가 closesocket을 했을 경우

		OVER_EX* over_ex = reinterpret_cast<OVER_EX*> (p_over);

		/*if( true == over_ex->is_recv ) {
			if( over_ex->buffer[1] == CS_ID )
				send_connect_result( key, over_ex->buffer );*/
				//else
		ProcessPacket( key, over_ex->buffer ); // key가 보낸 패킷을 처리해라.



		DWORD flags = 0;
		memset( &over_ex->over, 0x00, sizeof( WSAOVERLAPPED ) ); // 0으로 초기화 후 재사용.
		WSARecv( client_s, over_ex->wsabuf, 1, 0, &flags, &over_ex->over, 0 );
	}
	//else 
	{
		delete over_ex;
	}

}

void ProcessPacket( int id, void* buf )
{
	char* packet = reinterpret_cast<char*> (buf); // 내용 읽기위해 타입 캐스팅
	short x = clients[id]->x;
	short y = clients[id]->y;

	switch( packet[1] ){
	case CS_UP:
		if( y > 0 ) --y;
		break;
	case CS_DOWN:
		if( y < WORLD_HEIGHT - 1 ) ++y;
		break;
	case CS_LEFT:
		if( x > 0 ) --x;
		break;
	case CS_RIGHT:
		if( x < WORLD_WIDTH - 1 ) ++x;
		break;
	default: cout << "Invalid Packet Type Error\n";
		while( 1 );
	}

	// Update Position
	clients[id]->x = x;
	clients[id]->y = y;

	set<int> new_vl;
	for( auto& cl : clients ){
		int other = cl.second->id;
		if( id == other ) continue;
		if( is_near( id, other ) )
			new_vl.insert( other );
	}

	for( auto& cl : old_vl ){
		if( 0 != new_vl.count( cl ) ){
			if( !Is_NPC( cl ) )
				send_pos_packet( cl, id );
		}
		else{// old 엔 있고 new엔 없다
			send_remove_player_packet( id, cl );
			if( !Is_NPC( cl ) )
				send_remove_player_packet( cl, id );
		}
	}

	for( auto cl : new_vl ){
		if( 0 == old_Vl.count( cl ) ){
			// new엔 있고 old엔 없다.
			send_put_player_packet( id, cl );
			if( !Is_NPC( cl ) )
				send_put_player_packet( cl, id );
		}
	}

	// 시야 리스트 업데이트
	for( auto& cl : clients ){

		if( true == is_near( id, cl.first ) ){
			if( false == is_near_id( id, cl.first ) ){
				send_put_player_packet( id, cl.first );
				send_put_player_packet( cl.first, id );
			}
			else
				send_pos_packet( cl.first, id );
		}
		else
			if( true == is_near_id( id, cl.first ) ){
				send_remove_player_packet( id, cl.first );
				send_remove_player_packet( cl.first, id );
			}

	}
}

void send_packet( int id, void* buf )
{
	char* packet = reinterpret_cast<char*>(buf);
	int packet_size = packet[0];


	OVER_EX* send_over = new OVER_EX;
	memset( send_over, 0x00, sizeof( OVER_EX ) );
	send_over->is_recv = false; // recv 아니다~
	memcpy( send_over->buffer, packet, packet_size );
	send_over->wsabuf[0].buf = send_over->buffer;
	send_over->wsabuf[0].len = packet_size;
	int retval = WSASend( clients[id]->socket, send_over->wsabuf, 1, 0, 0, &send_over->over, 0 );
	if( retval != 0 ){
		int err_no = WSAGetLastError();
		if( WSA_IO_PENDING != err_no )
			err_display( "WSASend()" );
	}
}

void send_login_ok_packet( int id )
{
	sc_packet_login_ok packet;
	packet.id = id;
	packet.size = sizeof( packet );
	packet.type = SC_LOGIN_OK;
	send_packet( id, &packet );
}

void send_login_not_ok_packet( int id )
{
	sc_packet_login_not_ok packet;
	packet.id = id;
	packet.size = sizeof( packet );
	packet.type = SC_LOGIN_NOT_OK;
	send_packet( id, &packet );
}

void send_put_player_packet( int client, int new_id )
{
	sc_packet_put_player packet;
	packet.id = new_id;
	packet.size = sizeof( packet );
	packet.type = SC_PUT_PLAYER;
	packet.x = clients[new_id]->x;
	packet.y = clients[new_id]->y;
	send_packet( client, &packet );

	lock_guard<mutex> lg{ clients[client]->near_lock };
	clients[client]->near_id.insert( new_id );
}

void send_remove_player_packet( int client, int leaver )
{
	sc_packet_remove_player packet;
	packet.id = leaver;
	packet.size = sizeof( packet );
	packet.type = SC_REMOVE_PLAYER;
	send_packet( client, &packet );

	lock_guard<mutex> lg{ clients[client]->near_lock };
	clients[client]->near_id.erase( leaver );
}

void send_pos_packet( int client, int mover )
{
	sc_packet_pos packet;
	packet.id = mover;
	packet.size = sizeof( packet );
	packet.type = SC_POS;
	packet.x = clients[mover]->x;
	packet.y = clients[mover]->y;
	send_packet( client, &packet );

	lock_guard<mutex> lg{ clients[client]->near_lock };
	if( clients[client]->near_id.count( mover );
}

//void send_connect_result( int id, void* buf )
//{
//	// string을 wstring으로 변환하는 것 찾기
//	char* buffer = reinterpret_cast<char*>(buf);
//	string user_name_a = buffer;
//	wstring user_name_w;
//	//user_name_a.assign(buffer[2], buffer[len-2]);
//	clients[id]->user_name.assign( user_name_a.begin() + 2, user_name_a.end() );
//	user_name_w.assign( clients[id]->user_name.begin(), clients[id]->user_name.end() );
//
//	POS pos = db_mgr.GetPos( user_name_w );
//	if( pos.x < 0 && pos.y < 0 ){
//		send_login_not_ok_packet( id );
//		cout << "등록되지 않은 ID로 접속을 시도한 클라이언트가 있습니다." << endl;
//	}
//	else{
//		cout << pos.x << ", " << pos.y << endl;
//		send_login_ok_packet( id );
//		clients[id]->x = pos.x;
//		clients[id]->y = pos.y;
//
//		for( auto& cl : clients ){
//			int other_player = cl.first;
//			if( true == is_near( other_player, id ) ){
//				if( Is_NPC( other_player ) == false )
//					send_put_player_packet( other_player, id );	// npc 아닐때만 보내라.
//				if( other_player != id )
//					send_put_player_packet( id, other_player );
//			}
//		}
//	}
//}

bool is_near( int a, int b )
{
	if( abs( clients[a]->x - clients[b]->x ) < VIEW_RANGE && abs( clients[a]->y - clients[b]->y ) < VIEW_RANGE )
		return true;
	else
		return false;
}

bool is_near_id( int player, int other )
{
	lock_guard <mutex>  gl{ clients[player]->near_lock };
	return (0 != clients[player]->near_id.count( other ));
}

void Create_NPC()
{
	for( int npc_id = NPC_ID_START; npc_id < NPC_ID_START + NUM_NPC; ++npc_id )
	{
		clients[npc_id] = new SOCKETINFO;
		clients[npc_id]->id = npc_id;
		clients[npc_id]->x = rand() % WORLD_WIDTH;
		clients[npc_id]->y = rand() % WORLD_HEIGHT;
		// RECV , 시야처리 필요없다.
		clients[npc_id]->socket = -1;
	}
}

void do_ai()
{
	for( auto& npc : clients ){
		if( false == Is_NPC( npc.second->id ) )
			continue;
		int x = npc.second->x;
		int y = npc.second->y;

		set<int> old_view_list;
		for( auto& obj : clients )
			if( is_near( npc.second->id, obj.second->id ) )
				old_view_list.insert( obj.second->id );

		switch( rand() % 4 ){
		case 0:
			if( y > 0 )y--;
			break;
		case 1:
			if( (WORLD_HEIGHT - 1) > y ) y++;
			break;
		case 2:
			if( x > 0 ) x--;
			break;
		case 3:
			if( x < (WORLD_WIDTH - 1) )x++;
			break;
		}
		npc.second->x = x;
		npc.second->y = y;
		set<int> new_view_list;
		for( auto& obj : clients )
			if( is_near( npc.second->id, obj.second->id ) )
				old_view_list.insert( obj.second->id );
		for( auto& pc : clients )
		{
			if( true == Is_NPC( pc.second->id ) ) continue;
			// 시야처리
			if( !is_near( pc.second->id, npc.second->id ) ) continue;
			send_pos_packet( pc.second->id, npc.second->id );

			// 시야에서 벗어난 npc 빼 줘야함. 치야처리 넣어야함.
		}
	}
}

bool Is_NPC( int id )
{
	return id >= NPC_ID_START;
}