#pragma once
#include "MyThread.h"
#include "stdafx.h"
#include "DB_Mgr.h"
#include <string>

class Coord;
class WorkerThread final : public MyThread
{
public:
	WorkerThread();
	~WorkerThread();

public:
	void InitThread() override;
	void ProcThread() override;
	void JoinThread() override;

public:
	void DisconnectClient(const ULONG& key, const SOCKET& client_s);
	void ProcData(const int& id, char* buf, DWORD& numByte);
	void ProcessPacket(int id, void* buff);

	void OnEventRecv(const int& key, DWORD& num_byte, OVER_EX* over_ex);

	void OnEventAcceptEx(OVER_EX* over_ex);
	void OnEventPCMove(OVER_EX* over_ex, const int& key);
	void OnEventPCAttack(OVER_EX* over_ex, const int& key);
	void OnEventPCDie(OVER_EX* over_ex, const int& key);
	void OnEventPCRespawn(OVER_EX* over_ex, const int& key);
	void OnEventSavePlayerInfo(OVER_EX* over_ex, const int& key);
	void OnEventHeal(OVER_EX* over_ex, const int& key);

	void OnEventNPCMove(OVER_EX* over_ex, const int& key);
	void OnEventNotifyPCMove(OVER_EX* over_ex, const int& key);
	void OnEventNPCRespawn(OVER_EX* over_ex, const int& key);
	void OvEventNPCDie(OVER_EX* over_ex, const int& key);
	void OnEventNPCAttack(OVER_EX* over_ex, const int& key);
	void OnEventNPCPeace(OVER_EX* over_ex, const int& key);
	void OnEventNPCAttacked(OVER_EX* over_ex, const int& key);

public:
	bool Is_NPC(int id);
	bool is_near(int a, int b);
	bool is_near_npc(int a, int b);
	void FindNearSectors(set<pair<short, short>>& sectors, const Coord& pos);
	pair<short, short>GetSector(const Coord& pos);
	void MoveSector(const int& id, const pair<short, short>& mySector, const pair<short, short>& curSector);
	void ProcTeleport(const int& id, const Coord& dstPos);
	void ProcHeal(const int& id);
	void ProcNPCDead(const int& key);
	void ProcNPCRespawn(const int& key);
	void do_target_move(int npc_id, int target_id);
	void do_target_attack(int npc_id, int target_id);

	void ProcPCDead(int id, int npc_id);
	bool is_near_id(int player, int other);
	void MovePlayer(int id, char dir);

	void check_login_request(const wchar_t* id_str, int id, const bool& is_dummy);

private:
	DB_Mgr m_dbMgr;
};

