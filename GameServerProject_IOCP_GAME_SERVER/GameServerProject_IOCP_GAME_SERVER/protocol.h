#pragma once

constexpr int MAX_STR_LEN = 50;

#define WORLD_WIDTH		800
#define WORLD_HEIGHT	800

#define NPC_ID_START	20000	// 0 ~ 99 pc / 100 ~ npc
#define NUM_NPC			100000	// 100000�� default

#define SERVER_PORT		3500

#define SYSTEM_CHAT		-1

#define CS_LOGIN	1
#define CS_MOVE		2
#define CS_ATTACK	3
#define CS_CHAT		4
#define CS_LOGOUT	5
#define CS_TELEPORT 6
#define CS_HEAL		7

//===========================
#define DIR_UP			0
#define DIR_DOWN		1
#define DIR_LEFT		2
#define DIR_RIGHT		3
//===========================

#define SC_LOGIN_OK			1
#define SC_LOGIN_FAIL		2
#define SC_MOVE				3
#define SC_PUT_OBJECT		4
#define SC_REMOVE_OBJECT	5
#define SC_CHAT				6
#define SC_STAT_CHANGE		7
#define SC_TELEPORT			8
#define SC_DEAD				9

// dummy client
#define SC_ENTER			8
#define SC_LEAVE			9


// OBJECT TYPE
#define KNIGHT				0
#define ZOMBIE				1
#define ROGUE				2
#define SLAYER				3
#define DARKWARRIOR			4
#define PALADIN				5
#define DUMMY_KNIGHT		7

// PC TYPE
#define MY_CHARACTER		0
#define OTHER_PLAYER		6
#pragma pack(push ,1)
struct sc_packet_login_ok {
	unsigned char	size;
	char	type;
	int		id;
	short	hp;
	short	level;
	int		exp;
};

struct sc_packet_login_fail{
	unsigned char	size;
	char	type;
};

struct sc_packet_move {
	unsigned char	size;
	char	type;
	int		id;
	short	x, y;
	unsigned int move_time;
};

struct sc_packet_put_object {
	unsigned char	size;
	char	type;
	int		id;
	char	obj_type;
	short	x, y;
};

struct sc_packet_remove_object {
	unsigned char	size;
	char	type;
	int		id;
};

struct sc_packet_chat{
	unsigned char	size;
	char	type;
	int		id;
	wchar_t	chat[100];
};

struct sc_packet_stat_change{
	unsigned char	size;
	char	type;
	short	hp;
	short	level;
	short	atk;
	int		exp;
};

struct sc_packet_teleport {
	unsigned char size;
	char type;
	short x;
	short y;
};

struct sc_packet_dead {
	unsigned char size;
	char type;
	int id;
};

// =========================== Client To Server ==================================

struct cs_packet_login{
	unsigned char	size;
	char	type;
	wchar_t	name[10];
	char	is_dummy;
};

struct cs_packet_move{
	unsigned char size;
	char type;
	char direction;
	unsigned int move_time;
};

struct cs_packet_attack{
	unsigned char size;
	char type;
};

struct cs_packeet_chat{
	unsigned char size;
	char type;
	wchar_t char_str[100];
};

struct cs_packet_logout{
	unsigned char size;
	char type;
};

struct cs_packet_teleport{
	unsigned char size;
	char type;
	short x;
	short y;
};

struct cs_packet_heal {
	unsigned char size;
	char type;
};

#pragma pack (pop)