#pragma once
#include "Object.h"
class NPC :
    public Object
{
public:
    NPC() {};
    NPC(const int& _id, const wstring& _name, const char& _objType, const unsigned short& _hp,
        const unsigned char& _level, const unsigned int& _curExp, const unsigned short& _atk,
        const Coord& _pos) : Object(_id, _name, _objType, _hp, _hp, _level, _curExp, _curExp, _atk, _pos) {
        m_isAlive = true;
        m_isActive = false; // NPC AI On/Off 여부.
        
        m_initialPos = _pos;
        m_gotTarget = false;
    }
    virtual ~NPC() {}

public:
    void Move(Map* _map, const Coord& _targetPos, const int& _targetId);
    virtual void Attack(Object* _target) override;
    //virtual void Damaged(const unsigned short& _damage, bool& _isDead) override;
    virtual void Die() override;
    virtual void Revive() override;

public:
    inline void SetgotTarget(const bool& _gotTarget)        { m_gotTarget = _gotTarget; }
    inline bool GetgotTarget() const                        { return m_gotTarget; }
    inline Coord GetInitPos() const                         { return m_initialPos; }

private:
    Coord m_initialPos;     // 초기 스폰위치
    bool m_gotTarget;       // 타겟 유무
};

