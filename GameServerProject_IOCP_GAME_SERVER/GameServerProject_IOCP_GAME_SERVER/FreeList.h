#pragma once
#include "Singleton.h"
template<class T>
class FreeList
{
	struct Node {
		T item;
		Node* next;
	};
	Node* volatile m_head;
	Node* volatile m_tail;
	size_t m_size{};

	mutex m_lk;

public:
	FreeList() {
		m_head = m_tail = new Node();
	}
	~FreeList() {
		while (m_head->next != nullptr) {
			Node* ptr = m_head;
			m_head = m_head->next;
			delete ptr;
		}
	}

	bool CAS(Node* volatile& obj, Node* expected, Node* desire) {
		__int64 oldAddr = reinterpret_cast<__int64>(expected);
		__int64 newAddr = reinterpret_cast<__int64>(desire);
		return atomic_compare_exchange_strong((atomic_llong*)(&obj), &oldAddr, newAddr);
	}

	//T* NewOrRecycle() {
	//	// 다른 스레드에서 사용중인 노드를 또 반환해서 사용하게 만들 수 있다.
	//	m_lk.lock();
	//	if (m_head->next == nullptr) {		// 프리 리스트가 빈 상태
	//		Node* newNode = new Node(); // 객체 생성
	//		//newNode->next = m_head;		// 새 노드를 연결
	//		//m_head = newNode;			// 헤드를 옮기기
	//		++m_size;
	//		m_lk.unlock();
	//		return &newNode->item;		// 새로 생성한 객체를 반환
	//	}
	//	else {
	//		Node* oldhead = m_head;
	//		m_head = m_head->next;
	//		oldhead->next = nullptr;
	//		m_lk.unlock();
	//		return &oldhead->item;
	//	}
	//}

	T* NewOrRecycle() {
		while (true) {
			if (CAS(m_head, nullptr, m_head)) {		// 프리 리스트가 빈 상태
				Node* newNode = new Node(); // 객체 생성
				return &newNode->item;		// 새로 생성한 객체를 반환
			}
			else {
				if (CAS(m_head, nullptr, nullptr)) continue;
				Node* oldhead = m_head;	// 재사용할 노드
				Node* next = oldhead->next;
				if (CAS(m_head, oldhead, next)) { 	// 헤드를 옮기기
					next = nullptr;				// 재사용할 노드의 연결을 끊음.
					return &oldhead->item;
				}
			}
		}
	}

	//void Drop(T* obj) {
	//	// 사용을 끝낸 객체를 반납함.
	//	m_lk.lock();
	//	Node* dropped = reinterpret_cast<Node*>(obj);
	//	dropped->next = m_head;
	//	m_head = dropped;
	//	m_lk.unlock();
	//}

	void Drop(T* obj) {
		// 사용을 끝낸 객체를 반납함.
		Node* volatile dropped = reinterpret_cast<Node*>(obj);
		while (true) {
			Node* oldhead = m_head;
			dropped->next = oldhead;
			if (CAS(m_head, dropped->next, dropped)) return;
		}
	}
};

