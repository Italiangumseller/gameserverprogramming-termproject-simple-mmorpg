#include "stdafx.h"
#include "TimerThread.h"
#include "FreeList.h"

extern HANDLE g_iocp;
extern mutex timer_lock;
extern priority_queue<EVENT> timer_queue;
extern FreeList<OVER_EX> g_overFL;

void TimerThread::InitThread()
{
	m_myThread = thread([&]() {TimerThread::ProcThread(); });
}

void TimerThread::ProcThread()
{
	while (true) {
		timer_lock.lock();
		while (true == timer_queue.empty()) {
			timer_lock.unlock();
			this_thread::sleep_for(1ms);
			timer_lock.lock();
		}
		const EVENT& ev = timer_queue.top(); // 맨 앞에걸 보고 이벤트 처리할지 말지
		if (ev.wakeup_time > high_resolution_clock::now()) {
			timer_lock.unlock();
			this_thread::sleep_for(1ms); // 안그러면 타이머 스레드만 비지 웨이팅 걸림.
			// sleep_until(ev.wakeup_time) 쓰면 안 됨. 왜??  
			continue;
		}
		EVENT proc_ev = ev; // 그냥 팝하면 ev에 있는거 날라가서 복사해놓음.
		timer_queue.pop();
		timer_lock.unlock();

		// 이벤트가 발생하면 워커스레드에 떠넘기자.
		OVER_EX* over_ex = new OVER_EX; 
		//OVER_EX* over_ex = g_overFL.NewOrRecycle();
		ZeroMemory(over_ex, sizeof(OVER_EX));
		if (proc_ev.event_type == EV_MOVE_NPC) {
			over_ex->ev_type = EV_MOVE_NPC;
			*(int*)over_ex->net_buf = proc_ev.target_obj;
		}

		else if (proc_ev.event_type == EV_MOVE) {
			over_ex->ev_type = EV_MOVE;
			*(int*)over_ex->net_buf = proc_ev.target_obj;
		}

		else if (proc_ev.event_type == EV_HEAL) over_ex->ev_type = EV_HEAL;

		else if (proc_ev.event_type == EV_PEACE_NPC)
		{
			over_ex->ev_type = EV_PEACE_NPC;
			*(int*)over_ex->net_buf = proc_ev.target_obj;
		}

		else if (proc_ev.event_type == EV_DIE_MONSTER)
			over_ex->ev_type = EV_DIE_MONSTER;

		else if (proc_ev.event_type == EV_ATTACK)
			over_ex->ev_type = EV_ATTACK;

		else if (proc_ev.event_type == EV_RESPAWN_MONSTER)
			over_ex->ev_type = EV_RESPAWN_MONSTER;

		else if (proc_ev.event_type == EV_ATTACKED_NPC) {
			over_ex->ev_type = EV_ATTACKED_NPC;
			*(int*)over_ex->net_buf = proc_ev.target_obj;
		}
		else if (proc_ev.event_type == EV_ATTACK_NPC) {
			over_ex->ev_type = EV_ATTACK_NPC;
			*(int*)over_ex->net_buf = proc_ev.target_obj;
		}

		else if (proc_ev.event_type == EV_DIE) {
			over_ex->ev_type = EV_DIE;
			*(int*)over_ex->net_buf = proc_ev.target_obj;
		}

		else if (proc_ev.event_type == EV_RESPAWN) {
			over_ex->ev_type = EV_RESPAWN;
		}

		else if (proc_ev.event_type == EV_SAVE_PLAYER_INFO) over_ex->ev_type = EV_SAVE_PLAYER_INFO;

		else if (proc_ev.event_type == EV_PLAYER_MOVE_NOTIFY) {
			over_ex->ev_type = EV_PLAYER_MOVE_NOTIFY;
			*(int*)over_ex->net_buf = proc_ev.target_obj;
		}

		else {
			cout << "Unknown Event Type Error!" << endl;
			while (1);
		}

		PostQueuedCompletionStatus(g_iocp, 1, proc_ev.obj_id, &over_ex->over);
	}
}

void TimerThread::JoinThread()
{
	m_myThread.join();
}
