#pragma once
#include <list>
#include <algorithm>
#include <iostream>
#include <fstream>
#include "Coord.h"
#include "Map.h"



class Node {
public:
	bool operator == (const Node& o) { return pos == o.pos; }
	bool operator == (const Coord& o) { return pos == o; }
	bool operator < (const Node& o) { return dist + cost < o.dist + o.cost; }
	Coord pos, parent;
	int dist, cost;
};

class aStar {
public:
	aStar() {
		m_neighbours[0] = Coord(0, -1);
		m_neighbours[1] = Coord(-1, 0);
		m_neighbours[2] = Coord(0, 1);
		m_neighbours[3] = Coord(1, 0);
	}

	int calcDist(const Coord& p) {
		int x = m_endPoint.x - p.x, y = m_endPoint.y - p.y;
		return(x * x + y * y);
	}

	bool isValid(Coord& p) {
		bool isvalid = (p.x > -1 && p.y > -1 && p.x < m_map->w&& p.y < m_map->h 
			&& m_map->operator()(p.x, p.y) != 4);
		return isvalid;
	}

	bool existPoint(Coord& p, int cost) {
		std::list<Node>::iterator i;
		i = std::find(m_closedNodes.begin(), m_closedNodes.end(), p);
		if (i != m_closedNodes.end()) {
			if ((*i).cost + (*i).dist < cost) return true;
			else { m_closedNodes.erase(i); return false; }
		}
		i = std::find(m_openNodes.begin(), m_openNodes.end(), p);
		if (i != m_openNodes.end()) {
			if ((*i).cost + (*i).dist < cost) return true;
			else { m_openNodes.erase(i); return false; }
		}
		return false;
	}

	bool fillOpen(Node& n) {
		int nc, dist;
		Coord neighbour;

		for (int x = 0; x < 4; x++) {
			// one can make diagonals have different cost
			neighbour = n.pos + m_neighbours[x];
			if (neighbour == m_endPoint) return true;

			// 장애물 처리
			if (isValid(neighbour)) {
				nc = 1 + n.cost;
				dist = calcDist(neighbour);
				if (!existPoint(neighbour, nc + dist)) {
					Node m;
					m.cost = nc; m.dist = dist;
					m.pos = neighbour;
					m.parent = n.pos;
					m_openNodes.push_back(m);
				}

			}
		}
		return false;
	}

	bool search(const Coord& startPt, const Coord& endPt, Map* mp) {
		Node node; m_endPoint = endPt; m_startPoint = startPt;
		m_map = mp;
		node.cost = 0; node.pos = startPt; node.parent = 0; node.dist = calcDist(startPt);
		m_openNodes.push_back(node);
		while (!m_openNodes.empty()) {
			Node n = m_openNodes.front();
			m_openNodes.pop_front();
			m_closedNodes.push_back(n);
			if (fillOpen(n)) 
				return true;
		}
		return false;
	}

	int path(std::list<Coord>& path) {
		path.push_front(m_endPoint);
		int cost = 1 + m_closedNodes.back().cost;
		path.push_front(m_closedNodes.back().pos);
		Coord parent = m_closedNodes.back().parent;

		for (std::list<Node>::reverse_iterator i = m_closedNodes.rbegin(); i != m_closedNodes.rend(); i++) {
			if ((*i).pos == parent && !((*i).pos == m_startPoint)) {
				path.push_front((*i).pos);
				parent = (*i).parent;
			}
		}
		path.push_front(m_startPoint);
		return cost;
	}

	Map* m_map; Coord m_endPoint, m_startPoint;
	Coord m_neighbours[4];
	std::list<Node> m_openNodes;
	std::list<Node> m_closedNodes;
};