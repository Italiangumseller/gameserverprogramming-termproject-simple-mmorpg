#include "stdafx.h"
#include "ThreadHandler.h"
#include "DB_Mgr.h"
#include "Astar.h"
#include "Server.h"
#include "WorkerThread.h"
#include "Knight.h"
#include "NPC.h"
#include "FreeList.h"

extern HANDLE g_iocp;
extern array<SOCKETINFO, NPC_ID_START + NUM_NPC> g_clients;
extern mutex timer_lock;
extern priority_queue<EVENT> timer_queue;
extern unordered_set<int> g_objectSectors[SECTOR_NUM_ROW][SECTOR_NUM_COL];

extern FreeList<OVER_EX> g_overFL;

int new_user_id = 0;

//int g_world[WORLD_HEIGHT][WORLD_WIDTH];

void luaError(lua_State* L, const char* fmt, ...) {
	va_list argp;
	va_start(argp, fmt);
	vfprintf(stderr, fmt, argp);
	va_end(argp);
	lua_close(L);
	exit(EXIT_FAILURE);
}

int lua_get_x_position(lua_State* L)
{
	int npc_id = (int)lua_tonumber(L, -1);
	lua_pop(L, 2);
	int x = g_clients[npc_id].obj->GetPos().x;
	lua_pushnumber(L, x);
	return 1;
}

int lua_get_y_position(lua_State* L)
{
	int npc_id = (int)lua_tonumber(L, -1);
	lua_pop(L, 2);
	int y = g_clients[npc_id].obj->GetPos().y;
	lua_pushnumber(L, y);
	return 1;
}

int lua_send_chat_packet(lua_State* L)
{
	int player_id = (int)lua_tonumber(L, -3);
	int chatter_id = (int)lua_tonumber(L, -2);
	string msg = (string)lua_tostring(L, -1);

	lua_pop(L, 4);
	wstring wmsg;
	wmsg.assign(msg.begin(), msg.end());
	//cout << "CHAT from = " << chatter_id << " to = " << player_id << ", [ " << msg << " ]" << endl;
	Server::get()->send_chat_packet(player_id, chatter_id, wmsg);
	return 0;
}

int lua_target_move(lua_State* L)
{
	int npc_id = (int)lua_tonumber(L, -2);
	int target_id = (int)lua_tonumber(L, -1);
	lua_pop(L, 3);

	EVENT ev{ npc_id, high_resolution_clock::now() + 1s, EV_MOVE_NPC, target_id };
	Server::get()->add_timer(ev);
	return 0;
}

int lua_move_to_init_pos(lua_State* L)
{
	int npc_id = (int)lua_tonumber(L, -1);
	lua_pop(L, 2);

	EVENT ev{ npc_id, high_resolution_clock::now() + 1s, EV_MOVE_NPC, npc_id };
	Server::get()->add_timer(ev);
	return 0;
}


Server::Server()
{
}

Server::~Server()
{
	closesocket(m_listenSock);
	WSACleanup();
}

void Server::Create_NPC()
{
	cout << "Initializing NPC!" << endl;
	//ofstream out{ "NPC.txt", ios::binary };
	M_INFO info;
	static int npc_id = 0;
	ifstream in("NPC.txt", ios::binary);
	for (npc_id = NPC_ID_START; npc_id < NPC_ID_START + NUM_NPC; ++npc_id) {
		in.read((char*)&info, sizeof(info));

		string str{ info.id_str };
		wstring wstrName{};
		wstrName.assign(str.begin(), str.end());
		g_clients[npc_id].obj = new NPC(npc_id, wstrName, info.monster_type, info.hp, info.level, info.exp, info.atk, Coord(info.x, info.y));
		g_clients[npc_id].id = npc_id;
		g_clients[npc_id].socket = -1;
		g_clients[npc_id].is_connected = true;

		if (info.x >= 800) info.x = WORLD_WIDTH - 1;
		if (info.y >= 800) info.y = WORLD_HEIGHT - 1;

		short row = g_clients[npc_id].mySector.first = info.x / SECTOR_WIDTH;
		short col = g_clients[npc_id].mySector.second = info.y / SECTOR_HEIGHT;
		g_objectSectors[row][col].insert(npc_id);

		lua_State* L = g_clients[npc_id].L = luaL_newstate();
		luaL_openlibs(L);
		if (info.monster_type == PALADIN)
			luaL_loadfile(L, "NPC_paladin.lua");
		else
			luaL_loadfile(L, "monster.lua");

		lua_pcall(L, 0, 0, 0);
		lua_getglobal(L, "set_npc_id"); // 얘를 호출해야함.
		lua_pushnumber(L, npc_id);	// npc_id 세팅
		lua_pcall(L, 1, 0, 0);		// 함수 호출 가상 머신 세팅 완료

		// lua 함수와 c 함수를 매칭시킴 lua_resister
		lua_register(L, "API_get_x_position", lua_get_x_position);
		lua_register(L, "API_get_y_position", lua_get_y_position);
		lua_register(L, "API_send_chat_packet", lua_send_chat_packet);
		lua_register(L, "API_target_move", lua_target_move);
		lua_register(L, "API_move_to_init_pos", lua_move_to_init_pos);
	}



	/*npc 파일로 만드는 부분*/
	//random_device rd;
	//uniform_int_distribution<> zombieuid( 0, 200 );
	//uniform_int_distribution<> zombie_leveluid( 1, 10 );
	//
	//uniform_int_distribution<> rogueuid6( 200, 400 );
	//uniform_int_distribution<> rogueuid2_X( 200, 400 );
	//uniform_int_distribution<> rogueuid2_Y( 0, 200 );
	//uniform_int_distribution<> rogueuid5_X( 0, 200 );
	//uniform_int_distribution<> rogueuid5_Y( 200, 400 );
	//uniform_int_distribution<> rogue_leveluid( 8, 20 );
	//
	//uniform_int_distribution<> slayeruid11( 400, 600 );
	//uniform_int_distribution<> slayeruid910_X( 0, 400 );
	//uniform_int_distribution<> slayeruid910_Y( 400, 600 );
	//uniform_int_distribution<> slayeruid37_X( 400, 600 );
	//uniform_int_distribution<> slayeruid37_Y( 0, 400 );
	//uniform_int_distribution<> slayer_leveluid( 18, 30 );
	//
	//uniform_int_distribution<> darkwarrioruid( 600, 800 );
	//uniform_int_distribution<> darkwarrioruid1315_X( 0, 600 );
	//uniform_int_distribution<> darkwarrioruid1315_Y( 600, 800 );
	//uniform_int_distribution<> darkwarrioruid412_X( 600, 800 );
	//uniform_int_distribution<> darkwarrioruid412_Y( 0, 600 );
	//uniform_int_distribution<> darkwarrior_leveluid( 28, 42 );
	//
	//uniform_int_distribution<> paladinuid( 50, 800 );
	//uniform_int_distribution<> paladin_leveluid( 1, 40 );
	//{
	//	// ZOMBIE
	//	for( npc_id = NPC_ID_START; npc_id < NPC_ID_START + 10000; ++npc_id ){
	//		ZeroMemory( &info, sizeof( info ) );
	//		info.level = zombie_leveluid( rd );
	//		info.hp = info.level * 9;
	//		info.atk = info.level * 2;
	//		info.monster_type = ZOMBIE;
	//		info.exp = info.level * info.level * 5 * 2;
	//		string name_lv = "LV. " + to_string( info.level ) + " Zombie";
	//		strcpy( info.id_str, name_lv.c_str() );
	//
	//		info.x = zombieuid( rd );
	//		info.y = zombieuid( rd );
	//		if( info.x <= 50 && info.y <= 50 ){
	//			info.x += 50; 
	//			info.y += 50;
	//		}
	//
	//		out.write( (char*)&info, sizeof( info ) );
	//	}
	//
	//	// ROGUE
	//	int R_cnt = 0;
	//	for( npc_id; npc_id < NPC_ID_START + 25000; ++npc_id ){
	//		ZeroMemory( &info, sizeof( info ) );
	//		info.level = rogue_leveluid( rd );
	//		info.hp = info.level * 7;
	//		info.atk = info.level * 3;
	//		info.monster_type = ROGUE;
	//		info.exp = info.level * info.level * 5 * 2;
	//		string name_lv = "LV. " + to_string( info.level ) + " Rogue";
	//		strcpy( info.id_str, name_lv.c_str() );
	//		if(0 <= R_cnt && R_cnt < 5000 ){
	//			info.x = rogueuid2_X( rd );
	//			info.y = rogueuid2_Y( rd );
	//		}
	//		else if( 5000 <= R_cnt && R_cnt < 10000 ){
	//			info.x = rogueuid5_X( rd );
	//			info.y = rogueuid5_Y( rd );
	//		}
	//		else{
	//			info.x = rogueuid6( rd );
	//			info.y = rogueuid6( rd );
	//		}
	//
	//		++R_cnt;
	//		out.write( (char*)&info, sizeof( info ) );
	//	}
	//
	//	// SLAYER
	//	int S_cnt = 0;
	//	for( npc_id; npc_id < NPC_ID_START + 50000; ++npc_id ){
	//		ZeroMemory( &info, sizeof( info ) );
	//		info.level = slayer_leveluid( rd );
	//		info.hp = info.level * 12;
	//		info.atk = info.level * 3;
	//		info.monster_type = SLAYER;
	//		info.exp = info.level * info.level * 5 * 2;
	//		string name_lv = "LV. " + to_string( info.level ) + " Slayer";
	//		strcpy( info.id_str, name_lv.c_str() );
	//		if(0 <= S_cnt && S_cnt < 8000 ){
	//			info.x = slayeruid910_X( rd );
	//			info.y = slayeruid910_Y( rd );
	//		}
	//		else if( 8000 <= S_cnt && S_cnt < 16000 ){
	//			info.x = slayeruid37_X( rd );
	//			info.y = slayeruid37_Y( rd );
	//		}
	//		else{
	//			info.x = slayeruid11( rd );
	//			info.y = slayeruid11( rd );
	//		}
	//		++S_cnt;
	//		out.write( (char*)&info, sizeof( info ) );
	//	}
	//
	//	// DARKWARRIOR
	//	int D_cnt = 0;
	//	for( npc_id; npc_id < NPC_ID_START + 80000; ++npc_id ){
	//		ZeroMemory( &info, sizeof( info ) );
	//		info.level = darkwarrior_leveluid( rd );
	//		info.hp = info.level * 17;
	//		info.atk = info.level * 4;
	//		info.monster_type = DARKWARRIOR;
	//		info.exp = info.level * info.level * 5 * 2;
	//		string name_lv = "LV. " + to_string( info.level ) + " Darkwarrior";
	//		strcpy( info.id_str, name_lv.c_str() );
	//		info.x = darkwarrioruid( rd );
	//		info.y = darkwarrioruid( rd );
	//
	//		if( 0 <= D_cnt && D_cnt < 10000 ){
	//			info.x = darkwarrioruid1315_X( rd );
	//			info.y = darkwarrioruid1315_Y( rd );
	//		}
	//		else if( 10000 <= D_cnt && D_cnt < 20000 ){
	//			info.x = darkwarrioruid412_X( rd );
	//			info.y = darkwarrioruid412_Y( rd );
	//		}
	//		else{
	//			info.x = darkwarrioruid( rd );
	//			info.y = darkwarrioruid( rd );
	//		}
	//		++D_cnt;
	//		out.write( (char*)&info, sizeof( info ) );
	//	}
	//
	//	// PALADIN
	//	for( npc_id; npc_id < ((NUM_NPC / 5) * 5) + NPC_ID_START; ++npc_id ){
	//		ZeroMemory( &info, sizeof( info ) );
	//		info.level = paladin_leveluid( rd );
	//		info.hp = info.level * 15;
	//		info.atk = info.level * 5;
	//		info.monster_type = PALADIN;
	//		info.exp = info.level * info.level * 5;
	//		string name_lv = "LV. " + to_string( info.level ) + " paladin";
	//		strcpy( info.id_str, name_lv.c_str() );
	//		info.x = paladinuid( rd );
	//		info.y = paladinuid( rd );
	//
	//		out.write( (char*)&info, sizeof( info ) );
	//	}
	//}

	cout << "NPC Initialization finished.\n";
}

void Server::Create_PlayerSlot()
{
	cout << "Initializing Slot!" << endl;
	for (int user_id = 0; user_id < NPC_ID_START; ++user_id) {
		g_clients[user_id].obj = new Knight;
		g_clients[user_id].id = user_id;
		g_clients[user_id].socket = -1;
		g_clients[user_id].is_connected = false;
		g_clients[user_id].recv_over.wsabuf[0].len = MAX_BUFFER;
		g_clients[user_id].recv_over.wsabuf[0].buf = g_clients[user_id].recv_over.net_buf;
	}
	cout << "Slot Initialization finished.\n";
}

void Server::RunServer()
{
	m_threadHandler = new ThreadHandler;
	Create_PlayerSlot();
	Create_NPC();

	WSADATA WSAData;
	WSAStartup(MAKEWORD(2, 2), &WSAData);
	m_listenSock = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
	SOCKADDR_IN serverAddr;
	memset(&serverAddr, 0, sizeof(SOCKADDR_IN));
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(SERVER_PORT);
	serverAddr.sin_addr.S_un.S_addr = htonl(INADDR_ANY);
	::bind(m_listenSock, (struct sockaddr*)&serverAddr, sizeof(SOCKADDR_IN));
	listen(m_listenSock, 5);
	SOCKADDR_IN clientAddr;
	int addrLen = sizeof(SOCKADDR_IN);
	memset(&clientAddr, 0, addrLen);

	g_iocp = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, NULL, 0);
	m_threadHandler->CreateThread();

	CreateIoCompletionPort(reinterpret_cast<HANDLE>(m_listenSock), g_iocp, ACCEPT_KEY, 0);

	OVER_EX acceptOver{};
	acceptOver.ev_type = EV_ACCEPT;
	acceptOver.socket = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
	addrLen = sizeof(SOCKADDR_IN) + 16;
	if (FALSE == AcceptEx(m_listenSock, acceptOver.socket, acceptOver.net_buf, NULL, addrLen, addrLen, NULL, &acceptOver.over)) {
		int errNo{ WSAGetLastError() };
		if (errNo != WSA_IO_PENDING) error_display("Accept Error", errNo);
	}

	m_threadHandler->JoinThreads();
}

void Server::send_packet(int id, void* buf)
{
	char* packet = reinterpret_cast<char*>(buf);
	BYTE packet_size = packet[0];
	OVER_EX* send_over = new OVER_EX;
	//OVER_EX* send_over = g_overFL.NewOrRecycle();
	memset(send_over, 0x00, sizeof(OVER_EX));
	send_over->ev_type = EV_SEND;
	memcpy(send_over->net_buf, packet, packet_size);
	send_over->wsabuf[0].buf = send_over->net_buf;
	send_over->wsabuf[0].len = packet_size;
	int ret = WSASend(g_clients[id].socket, send_over->wsabuf, 1, 0, 0, &send_over->over, 0);
	if (0 != ret) {
		int err_no = WSAGetLastError();
		if (WSA_IO_PENDING != err_no)
			error_display("WSASend Error :", err_no);
	}
}

void Server::send_login_ok_packet(int id)
{
	sc_packet_login_ok packet{};
	packet.id = id;
	packet.size = sizeof(packet);
	packet.type = SC_LOGIN_OK;
	packet.hp = g_clients[id].obj->GetHp();
	packet.level = g_clients[id].obj->GetLevel();
	packet.exp = g_clients[id].obj->GetCurExp();
	send_packet(id, &packet);
}

void Server::send_login_fail_packet(int id)
{
	sc_packet_login_fail packet{};
	packet.size = sizeof(packet);
	packet.type = SC_LOGIN_FAIL;
	send_packet(id, &packet);
}

void Server::send_put_object_packet(int client, int new_id)
{
	sc_packet_put_object packet{};
	packet.id = new_id;
	packet.size = sizeof(packet);
	packet.type = SC_PUT_OBJECT;
	packet.obj_type = g_clients[new_id].obj->GetObjType();
	Coord pos{ g_clients[new_id].obj->GetPos() };
	packet.x = pos.x;
	packet.y = pos.y;
	send_packet(client, &packet);

	if (client == new_id) return;
	g_clients[client].near_lock.lock();
	g_clients[client].near_id.insert(new_id);
	g_clients[client].near_lock.unlock();
}

void Server::send_move_packet(int client, int mover)
{
	sc_packet_move packet{};
	packet.id = mover;
	packet.size = sizeof(packet);
	packet.type = SC_MOVE;
	Coord pos{ g_clients[mover].obj->GetPos() };
	packet.x = pos.x;
	packet.y = pos.y;
	packet.move_time = duration_cast<milliseconds>(high_resolution_clock::now().time_since_epoch()).count();

	g_clients[client].near_lock.lock_shared();
	const auto vl = g_clients[client].near_id;
	g_clients[client].near_lock.unlock_shared();
	if (0 != vl.count(mover) || client == mover) {
		send_packet(client, &packet);
	}
	else {
		send_put_object_packet(client, mover);
	}
}

void Server::send_remove_object_packet(int client, int leaver)
{
	sc_packet_remove_object packet{};
	packet.id = leaver;
	packet.size = sizeof(packet);
	packet.type = SC_REMOVE_OBJECT;
	send_packet(client, &packet);

	g_clients[client].near_lock.lock();
	g_clients[client].near_id.erase(leaver);
	g_clients[client].near_lock.unlock();
}

void Server::send_chat_packet(int client, int chatter, const wstring& msg)
{
	sc_packet_chat packet{};
	packet.id = chatter;
	packet.size = sizeof(packet);
	packet.type = SC_CHAT;
	wmemcpy(packet.chat, msg.c_str(), msg.length());
	send_packet(client, &packet);
}

void Server::send_stat_change_packet(int client)
{
	sc_packet_stat_change packet{};
	packet.size = sizeof(packet);
	packet.type = SC_STAT_CHANGE;
	packet.hp = g_clients[client].obj->GetMaxHp();
	packet.level = g_clients[client].obj->GetLevel();
	packet.atk = g_clients[client].obj->GetAtk();
	packet.exp = g_clients[client].obj->GetMaxExp();
}

void Server::send_teleport_packet(const int& client)
{
	sc_packet_teleport packet;
	packet.size = sizeof(sc_packet_teleport);
	packet.type = SC_TELEPORT;
	Coord pos = g_clients[client].obj->GetPos();
	packet.x = pos.x;
	packet.y = pos.y;
	send_packet(client, &packet);
}

void Server::sendDeadPacket(const int& client, const int& deadman)
{
	sc_packet_dead packet;
	packet.size = sizeof(packet);
	packet.type = SC_DEAD;
	packet.id = deadman;
	send_packet(client, &packet);
}

void Server::error_display(const char* msg, int err_no)
{
	WCHAR* lpMsgBuf;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM,
		NULL, err_no,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf, 0, NULL);
	cout << msg;
	wcout << L"에러 [" << err_no << "] " << lpMsgBuf << endl;
	//while (true);
	LocalFree(lpMsgBuf);
}

void Server::add_timer(EVENT& ev)
{
	timer_lock.lock();
	timer_queue.push(ev);
	timer_lock.unlock();
}