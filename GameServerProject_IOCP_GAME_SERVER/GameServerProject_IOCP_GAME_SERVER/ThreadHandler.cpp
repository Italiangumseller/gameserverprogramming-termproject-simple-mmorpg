#include "stdafx.h"
#include "ThreadHandler.h"
#include "Global.h"
#include "WorkerThread.h"
#include "TimerThread.h"

ThreadHandler::ThreadHandler()
{
	m_myThreads.reserve(NUM_WORKERTHREADS + 1);
}

ThreadHandler::~ThreadHandler()
{
}

void ThreadHandler::CreateThread()
{
	for (int i = 0; i < NUM_WORKERTHREADS; ++i) { 
		AddThread(new WorkerThread); 
	}

	AddThread(new TimerThread);
}

void ThreadHandler::JoinThreads()
{
	for (auto& thread : m_myThreads) {
		thread->JoinThread();
	}
}

void ThreadHandler::AddThread(MyThread* thread)
{
	thread->InitThread();
	m_myThreads.emplace_back(thread);
}
