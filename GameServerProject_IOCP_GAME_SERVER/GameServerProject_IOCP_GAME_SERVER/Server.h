#pragma once
#include "Singleton.h"
#include "Global.h"

class ThreadHandler;
class DB_Mgr;
class Server : public Singleton<Server>
{
public:
	Server();
	~Server();

public:
	void RunServer();
	void Create_PlayerSlot();
	void Create_NPC();

public:
	void send_packet(int id, void* buf);
	void send_login_ok_packet(int id);
	void send_login_fail_packet(int id);
	void send_put_object_packet(int client, int new_id);
	void send_move_packet(int client, int mover);
	void send_remove_object_packet(int client, int leaver);
	void send_chat_packet(int client, int chatter, const wstring& msg);
	void send_stat_change_packet(int client);
	void send_teleport_packet(const int& client);
	void sendDeadPacket(const int& client, const int& deadman);


public:
	void error_display(const char* msg, int err_no);
	void add_timer(EVENT& ev);


public:
	inline SOCKET GetListenSocket() const { return m_listenSock; }

private:
	ThreadHandler* m_threadHandler{};
	SOCKET m_listenSock{};
};

