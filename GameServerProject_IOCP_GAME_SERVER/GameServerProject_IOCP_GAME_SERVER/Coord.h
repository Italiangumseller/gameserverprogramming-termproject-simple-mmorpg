#pragma once

class Coord {
public:
	Coord(short a = 0, short b = 0) { x = a; y = b; }
	const Coord& operator()(short _x, short _y)
	{
		return Coord(x + _x, y + _y);
	}
	bool operator ==(const Coord& o) { return o.x == x && o.y == y; }
	Coord operator +(const Coord& o) { return Coord(o.x + x, o.y + y); }

	bool IsNeighbor(const Coord& o) {
		return ((o.x == x - 1 && o.y == y) || (o.x == x + 1 && o.y == y) || (o.x == x && o.y == y - 1) || (o.x == x && o.y == y + 1));
	}
	int GetDistance(const Coord& pos) {
		int xDst{ abs(x - pos.x) };
		int yDst{ abs(y - pos.y) };
		
		return xDst + yDst;
	}
	short x, y;
};
