#pragma once
#include <iostream>
#include <map>
#include <thread>
#include <set>
#include <unordered_set>
#include <mutex>
#include <shared_mutex>
#include <string>
#include <chrono>
#include<queue>
#include <array>
#include <random>
#include <concurrent_unordered_map.h>
#include <concurrent_unordered_set.h>
#include <WS2tcpip.h>
#include <MSWSock.h>


extern "C" {
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"
}

using namespace std;
using namespace chrono;
#include "protocol.h"

#pragma comment(lib, "Ws2_32.lib")
#pragma comment(lib, "mswsock.lib")
#pragma comment(lib, "lua53.lib")
