#include "stdafx.h"
#include "Knight.h"
#include "Map.h"

constexpr size_t PK_EXP_OFFSET = 500;
extern unsigned int MAX_EXP_PER_LEVEL[61];

Knight::Knight()
{
}

Knight::Knight(const int& _id, const wstring& _name, const char& _objType, const unsigned short& _hp, const unsigned short& _maxHp, const unsigned char& _level, const unsigned int& _curExp, const unsigned int& _maxExp, const unsigned short& _atk, const Coord& _pos)
	: Object(_id, _name, _objType, _hp, _maxHp, _level, _curExp, _maxExp, _atk, _pos)
{
	m_isAlive = true;
	m_isActive = false; // if false == Can Act
}

Knight::Knight(const Knight& _other) noexcept : Object(_other)
{
}

Knight& Knight::operator=(const Knight& _other) noexcept
{
	Object::operator=(_other);
	return *this;
}

Knight::~Knight()
{
}

void Knight::IncreaseExp(const unsigned short& _exp)
{
	// 누적 경험치로?
	m_curExp += _exp;
	if (m_maxExp <= m_curExp) {
		m_curExp -= m_maxExp;
		LevelUp();
	}
}

void Knight::DecreaseExp()
{
	size_t expDecreasement = (unsigned int)(m_maxExp * 0.3f);
	if (expDecreasement >= m_curExp) m_curExp = 0;
	else m_curExp -= expDecreasement;
}

void Knight::LevelUp()
{
	if (m_level >= 60) return;
	m_level++;
	m_maxExp = MAX_EXP_PER_LEVEL[m_level];
	m_maxHp += 500;
	m_hp = m_maxHp;
	m_atk += 50;
}

void Knight::TelePort(const Coord& _pos)
{
	//cout << "Player Teleported." << endl; 
	m_pos = _pos;
}

void Knight::Move(const Map& _map, const char& _dir)
{
	Coord pos{m_pos};
	switch (_dir) {
	case DIR_UP:
		pos.y--;
		break;
	case DIR_DOWN:
		pos.y++;
		break;
	case DIR_LEFT:
		pos.x--;
		break;
	case DIR_RIGHT:
		pos.x++;
		break;
	}

	if (_map.isValid(pos.x, pos.y)) 
		m_pos = pos;
}

void Knight::Attack(Object* _target)
{
	bool canGetExp{ false };
	_target->Damaged(m_id, m_atk, canGetExp, EV_DIE_MONSTER);
	
	if (canGetExp) { 
		unsigned int expIncreasement{};
		if (_target->GetObjType() == KNIGHT) expIncreasement = _target->GetLevel() * PK_EXP_OFFSET;
		else expIncreasement = _target->GetCurExp();
		IncreaseExp(expIncreasement); 
	}
}

void Knight::Die()
{
	bool expected{ true };
	if (atomic_compare_exchange_strong(&m_isAlive, &expected, false)) {
		//cout << "player id - " << m_id << " is dead." << endl;
		DecreaseExp();
		EVENT ev{ m_id, high_resolution_clock::now() + 10s, EV_RESPAWN, NULL };
		Server::get()->add_timer(ev);
	}
}

void Knight::Revive()
{
	bool expected{ false };
	if (atomic_compare_exchange_strong(&m_isAlive, &expected, true)) {
		//cout << "player id - " << m_id << " respawned" << endl;
	}
	if (m_objType == DUMMY_KNIGHT) {
		m_hp = m_maxHp;
	}
}
