my_id = -1

function set_npc_id(id)
	my_id = id
end

function event_player_move_notify (player_id) --플레이어 위치 받기
	--my_x = API_get_x_position(my_id)  --NPC의 위치는 게임서버가 관리 => 따로 얻어내야함. 부하가 적은 방법으로 선택해야함. (발생횟수가 적은 이벤트로 정해야함.)
	--my_y = API_get_y_position(my_id)

	--target_x = API_get_x_position(target_id)
	--target_y = API_get_y_position(target_id)

	API_send_chat_packet(player_id, my_id, "Crush him!!")
	API_target_move(my_id, player_id)
end

function event_peace(player_id)
	API_send_chat_packet(player_id, my_id, "Boring!")
	API_move_to_init_pos(my_id, my_id)
end
