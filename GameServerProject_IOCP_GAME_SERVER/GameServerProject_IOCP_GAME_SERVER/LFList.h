#pragma once
#include "Global.h"

class EBRNode {
public:
	int key;
	long long epoch;
	EBRNode* volatile next;

	EBRNode() { next = nullptr; }
	EBRNode(int x) {
		key = x;
		next = nullptr;
	}
	~EBRNode() {}
};

class alignas(64) EBRManager {
	EBRNode* head, * tail;
	EBRManager* ebrs;
	int counter;

public:
	long long localEpoch;
	EBRManager() { counter = 0; head = tail = nullptr; localEpoch = 0; }
	~EBRManager() {
		if (nullptr == tail) return;
		while (tail != head) {
			EBRNode* t = head;
			head = head->next;
			delete t;
		}
		delete head;
	}

	void InitEbrs(EBRManager p[]) {
		ebrs = p;
	}

	void RetireNode(EBRNode* p) {
		p->epoch = localEpoch;
		if (nullptr != tail) tail->next = p;
		tail = p;
		if (nullptr == head) head = tail;
	}

	long long GetLastEpoch() {
		long long oldestEpoch{ INT_MAX };
		for (int i = 0; i < NUM_WORKERTHREADS; ++i) {
			if (oldestEpoch > ebrs[i].localEpoch)
				oldestEpoch = ebrs[i].localEpoch;
		}
		return oldestEpoch;
	}

	EBRNode* GetNode(int key) {
		if (tail != head) {
			if (head->epoch < GetLastEpoch()) {
				EBRNode* e = head;
				head = e->next;
				e->key = key;
				e->next = nullptr;
				e->epoch = localEpoch;
				return e;
			}
		}
		return new EBRNode{ key };
	}

	void UpdateEpoch(long long* new_epoch) {
		counter++;
		if (counter < 1000) return;
		counter = 0;
		localEpoch = (*new_epoch)++;
	}
};

class LFList
{
	alignas(64) EBRNode* volatile head;
	alignas(64) EBRNode* volatile tail;
	long long g_epoch;
	EBRManager ebr[NUM_WORKERTHREADS];
public:
	LFList() {
		head = tail = new EBRNode();
		ebr[0].InitEbrs(ebr);
		g_epoch = 0;
	}

	~LFList() {
		Clear();
		delete head;
	}

	void ThreadInit()
	{
		for (auto& e : ebr) {
			e.InitEbrs(ebr);
		}
	}

	void Clear()
	{
		ThreadInit();
		while (head != tail) {
			EBRNode* toDelete = head;
			head = head->next;
			delete toDelete;
		}
	}

	bool CAS(EBRNode* volatile* next, EBRNode* oldNode, EBRNode* newNode) {
		return atomic_compare_exchange_strong(
			reinterpret_cast<volatile atomic_int*>(next),
			reinterpret_cast<int*>(&oldNode),
			reinterpret_cast<int>(newNode));
	}

	void Find(EBRNode*& pred, EBRNode*& curr, int key)
	{ // 포인터의 레터런스를 넘겨줘야한다.
	//retry:
	//	pred = head;
	//	curr = pred->next.GetAddr();
	//	// curr 를 함부로 사용할 수 없다.
	//	// 마킹되지 않은 curr만 읽어야함.
	//	while (true) {
	//		// curr가 마킹되었는지 확인부터 하자.
	//		bool is_removed{ false };
	//		EBRNode* succ = curr->next.GetAddr(&is_removed);

	//		// marking된 curr 지우기.
	//		while (is_removed == true) {
	//			if (pred->next.CAS(curr, succ, false, false)) {
	//				curr = succ;
	//				succ = curr->next.GetAddr(&is_removed);
	//			}
	//			else goto retry; // 충돌해서 처음부터 다시 돌아야함.
	//		}
	//		if (curr->key >= key) return;
	//		pred = curr;
	//		curr = succ;
	//	}

	}
};

