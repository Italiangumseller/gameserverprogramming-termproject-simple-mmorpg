my_id = -1

function set_npc_id(id)
	my_id = id
end

function event_player_move_notify(player_id)
	API_send_chat_packet(player_id, my_id, "Hello There!")
end

function event_attacked(player_id)
	API_send_chat_packet(player_id, my_id, "You will die!")
	API_target_move(my_id, player_id)
end

function event_peace(player_id)
	API_send_chat_packet(player_id, my_id, "Inner peace!")
	API_target_move(my_id, my_id)
end