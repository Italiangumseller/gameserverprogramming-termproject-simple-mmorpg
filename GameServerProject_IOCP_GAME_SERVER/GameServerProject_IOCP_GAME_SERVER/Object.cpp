#include "stdafx.h"
#include "Object.h"

Object::Object()
{
	memset(this, 0x00, sizeof(this));
}

Object::Object(const int& id, const wstring& _name, const char& _objType, const unsigned short& _hp, const unsigned short& _maxHp, const unsigned char& _level, const unsigned int& _curExp, const unsigned int& _maxExp, const unsigned short& _atk, const Coord& _pos)
{
	m_id = id;
	m_name = _name;
	m_objType = _objType;
	m_hp = _hp;
	m_maxHp = _maxHp;
	m_level = _level;
	m_curExp = _curExp;
	m_maxExp = _maxExp;
	m_atk = _atk;
	m_pos = _pos;
}

Object::Object(const Object& _other) noexcept
{
	m_id = _other.m_id;
	m_name = _other.m_name;
	m_objType = _other.m_objType;
	m_hp = _other.m_hp;
	m_maxHp = _other.m_maxHp;
	m_level = _other.m_level;
	m_curExp = _other.m_curExp;
	m_maxExp = _other.m_maxExp;
	m_atk = _other.m_atk;
	m_pos = _other.m_pos;
	m_isActive = _other.m_isActive;
	m_isAlive.store(_other.m_isAlive);
}

Object& Object::operator=(const Object& _other) noexcept
{
	m_id = _other.m_id;
	m_name = _other.m_name;
	m_objType = _other.m_objType;
	m_hp = _other.m_hp;
	m_maxHp = _other.m_maxHp;
	m_level = _other.m_level;
	m_curExp = _other.m_curExp;
	m_maxExp = _other.m_maxExp;
	m_atk = _other.m_atk;
	m_pos = _other.m_pos;
	m_isActive = _other.m_isActive;
	m_isAlive.store(_other.m_isAlive);

	return *this;
}

Object::Object(Object&& _other) noexcept
{
	m_id = _other.m_id;
	m_name = _other.m_name;
	m_objType = _other.m_objType;
	m_hp = _other.m_hp;
	m_maxHp = _other.m_maxHp;
	m_level = _other.m_level;
	m_curExp = _other.m_curExp;
	m_maxExp = _other.m_maxExp;
	m_atk = _other.m_atk;
	m_pos = _other.m_pos;
	m_isActive = _other.m_isActive;
	m_isAlive.store(_other.m_isAlive);
}

Object& Object::operator=(Object&& _other) noexcept
{
	m_id = _other.m_id;
	m_name = _other.m_name;
	m_objType = _other.m_objType;
	m_hp = _other.m_hp;
	m_maxHp = _other.m_maxHp;
	m_level = _other.m_level;
	m_curExp = _other.m_curExp;
	m_maxExp = _other.m_maxExp;
	m_atk = _other.m_atk;
	m_pos = _other.m_pos;
	m_isActive = _other.m_isActive;
	m_isAlive.store(_other.m_isAlive);

	return *this;
}

Object::~Object()
{
}

void Object::Damaged(const int& _attacker, const unsigned short& _damage, bool& _isDead, const EVENT_TYPE& _evType)
{
	if (m_hp <= _damage) {
		_isDead = true;
		if (m_isAlive) {
			EVENT ev{ m_id, high_resolution_clock::now(), _evType, _attacker };
			Server::get()->add_timer(ev);
			m_hp = 0;
			return;
		}
	}
	else m_hp -= _damage;
}
