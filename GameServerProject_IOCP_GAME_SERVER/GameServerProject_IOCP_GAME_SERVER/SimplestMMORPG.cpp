#include "stdafx.h"
#include "Server.h"
#include "Astar.h"
#include "Global.h"
#include "FreeList.h"

// 전역 변수 어떻게 관리 하는게 좋은지 찾아보자. 어디에 두고 어떻게 다른 cpp 파일에서 사용? extern.h 사용했던 것처럼?
mutex timer_lock; // 멀쓰니까 락 필요!
array<SOCKETINFO, NPC_ID_START + NUM_NPC> g_clients;
HANDLE	g_iocp;
Map g_world;
unordered_set<int> g_objectSectors[SECTOR_NUM_ROW][SECTOR_NUM_COL];
priority_queue<EVENT> timer_queue;

FreeList<OVER_EX> g_overFL;

unsigned int MAX_EXP_PER_LEVEL[61]{ 0,
	1000,
};

int main()
{
	wcout.imbue(std::locale("korean"));

	for (int i = 2; i < MAX_LEVEL; ++i) {
		MAX_EXP_PER_LEVEL[i] = MAX_EXP_PER_LEVEL[i - 1] * 2;
	}
	Server* server = Server::get();
	server->RunServer();
}