#include "stdafx.h"
#include "WorkerThread.h"
#include "DB_Mgr.h"
#include "Server.h"
#include "Astar.h"
#include "Knight.h"
#include "NPC.h"
#include "FreeList.h"

extern HANDLE g_iocp;
extern array<SOCKETINFO, NPC_ID_START + NUM_NPC> g_clients;
extern Map g_world;
extern unordered_set<int>g_objectSectors[SECTOR_NUM_ROW][SECTOR_NUM_COL];
extern unsigned int MAX_EXP_PER_LEVEL[61];
extern void luaError(lua_State* L, const char* fmt, ...);
shared_mutex g_sectorlk[SECTOR_NUM_ROW][SECTOR_NUM_COL];

extern FreeList<OVER_EX> g_overFL;


WorkerThread::WorkerThread()
{
}

WorkerThread::~WorkerThread()
{
}

void WorkerThread::InitThread()
{
	m_myThread = thread([&]() {WorkerThread::ProcThread(); });
}

void WorkerThread::JoinThread()
{
	m_myThread.join();
}

void WorkerThread::ProcThread()
{
	while (true) {
		DWORD num_byte{};
		ULONG key{};
		PULONG p_key = &key;
		WSAOVERLAPPED* p_over{};

		if (FALSE == GetQueuedCompletionStatus(g_iocp, &num_byte, (PULONG_PTR)p_key, &p_over, INFINITE)) {
			Server::get()->error_display("GQCS Error", WSAGetLastError());
			continue;
		}

		OVER_EX* over_ex = reinterpret_cast<OVER_EX*> (p_over);

		switch (over_ex->ev_type) {
		case EV_RECV:
			OnEventRecv(key, num_byte, over_ex);
			break;
		case EV_SEND:
			//delete over_ex;
			delete over_ex;
			break;
		case EV_ACCEPT:
			OnEventAcceptEx(over_ex);
			break;
		case EV_MOVE:
			OnEventPCMove(over_ex, key);
			break;
		case EV_ATTACK:
			OnEventPCAttack(over_ex, key);
			break;
		case EV_DIE:
			OnEventPCDie(over_ex, key);
			break;
		case EV_RESPAWN:
			OnEventPCRespawn(over_ex, key);
			break;
		case EV_SAVE_PLAYER_INFO:
			OnEventSavePlayerInfo(over_ex, key);
			break;
		case EV_HEAL:
			OnEventHeal(over_ex, key);
			break;
		case EV_MOVE_NPC:
			OnEventNPCMove(over_ex, key);
			break;
		case EV_ATTACK_NPC:
			OnEventNPCAttack(over_ex, key);
			break;
		case EV_DIE_MONSTER:
			OvEventNPCDie(over_ex, key);
			break;
		case EV_RESPAWN_MONSTER:
			OnEventNPCRespawn(over_ex, key);
			break;
		case EV_PLAYER_MOVE_NOTIFY:
			OnEventNotifyPCMove(over_ex, key);
			break;
		case EV_ATTACKED_NPC:
			OnEventNPCAttacked(over_ex, key);
			break;
		case EV_PEACE_NPC:
			OnEventNPCPeace(over_ex, key);
			break;
		default:
			cout << "Unknown Event Type: " << over_ex->ev_type << endl;
			while (true);
		}
	}
}

void WorkerThread::OnEventNPCRespawn(OVER_EX* over_ex, const int& key)
{
	g_clients[key].is_connected = true;
	ProcNPCRespawn(key);
	delete over_ex;
}

void WorkerThread::OvEventNPCDie(OVER_EX* over_ex, const int& key)
{
	// 여기서 key(죽은 NPC) 근처의 pc에게 remove packet 전송하기
	ProcNPCDead(key);
	delete over_ex;
}

void WorkerThread::OnEventNPCAttack(OVER_EX* over_ex, const int& key)
{
	int target_id = *(int*)(over_ex->net_buf);
	do_target_attack(key, target_id);
	delete over_ex;
}

void WorkerThread::OnEventNPCPeace(OVER_EX* over_ex, const int& key)
{
	int player_id = *(int*)(over_ex->net_buf);
	lua_State* L = g_clients[key].L;
	lua_getglobal(L, "event_peace");
	lua_pushnumber(L, player_id);
	lua_pcall(L, 1, 0, 0);
	static_cast<NPC*>(g_clients[key].obj)->SetgotTarget(false);
	static_cast<NPC*>(g_clients[key].obj)->SetActive(false);
	delete over_ex;
}

void WorkerThread::OnEventNPCAttacked(OVER_EX* over_ex, const int& key)
{
	NPC* npc{ static_cast<NPC*>(g_clients[key].obj) };
	if (npc->GetgotTarget() == false) {
		npc->SetgotTarget(true);
		int player_id = *(int*)(over_ex->net_buf);
		lua_State* L = g_clients[key].L;
		lua_getglobal(L, "event_attacked");
		lua_pushnumber(L, player_id);
		lua_pcall(L, 1, 0, 0);
	}
	delete over_ex;
}

void WorkerThread::OnEventNotifyPCMove(OVER_EX* over_ex, const int& key)
{
	int player_id = *(int*)(over_ex->net_buf);
	lua_State* L = g_clients[key].L;
	NPC* npc{ static_cast<NPC*>(g_clients[key].obj) };
	if (npc->GetgotTarget()) {
		delete over_ex;
		return;
	}
	npc->SetActive(true);
	if (npc->GetgotTarget() == false) {
		if (npc->GetObjType() != PALADIN) {
			npc->SetgotTarget(true);
			lua_getglobal(L, "event_player_move_notify");
			lua_pushnumber(L, player_id);
			if (0 != lua_pcall(L, 1, 0, 0))
				luaError(L, "error running function 'event_player_move_notify'", player_id);
		}
	}
	delete over_ex;
}

void WorkerThread::OnEventNPCMove(OVER_EX* over_ex, const int& key)
{
	int target_id = *(int*)(over_ex->net_buf);
	do_target_move(key, target_id);
	delete over_ex;
}

void WorkerThread::OnEventHeal(OVER_EX* over_ex, const int& key)
{
	// 자힐
	ProcHeal(key);
	delete over_ex;
}

void WorkerThread::OnEventSavePlayerInfo(OVER_EX* over_ex, const int& key)
{
	wstring name{ g_clients[key].obj->GetName() };
	Coord pos{ g_clients[key].obj->GetPos() };
	m_dbMgr.SetPos(name, pos.x, pos.y);
	EVENT ev{ key, high_resolution_clock::now() + 3min, EV_SAVE_PLAYER_INFO, 0 };
	Server::get()->add_timer(ev);
	delete over_ex;
}

void WorkerThread::OnEventPCRespawn(OVER_EX* over_ex, const int& key)
{
	g_clients[key].obj->Revive();
	if (g_clients[key].obj->GetObjType() == DUMMY_KNIGHT)
		ProcTeleport(key, Coord{ rand() % 800, rand() % 800 });
	else ProcTeleport(key, Coord{ 4, 4 });
	delete over_ex;
}

void WorkerThread::OnEventPCDie(OVER_EX* over_ex, const int& key)
{
	// 경험치 깎고 초기 위치로 보내기.
	int killer_id = *(int*)over_ex->net_buf;
	ProcPCDead(key, killer_id);
	delete over_ex;
}

void WorkerThread::OnEventPCMove(OVER_EX* over_ex, const int& key)
{
	char mvDir = *(int*)over_ex->net_buf;
	MovePlayer(key, mvDir);
	delete over_ex;
}

void WorkerThread::OnEventAcceptEx(OVER_EX* over_ex)
{
	int user_id{};// = new_user_id++;
	for (int i = 0; i < NPC_ID_START; ++i)
		if (g_clients[i].is_connected == false)
		{
			user_id = i;
			break;
		}
	g_clients[user_id].id = user_id;
	g_clients[user_id].socket = over_ex->socket;
	g_clients[user_id].is_connected = true;
	g_clients[user_id].recv_over.wsabuf[0].len = MAX_BUFFER;
	g_clients[user_id].recv_over.wsabuf[0].buf = g_clients[user_id].recv_over.net_buf;
	g_clients[user_id].recv_over.ev_type = EV_RECV;

	SOCKET clSocket{ g_clients[user_id].socket };
	CreateIoCompletionPort(reinterpret_cast<HANDLE>(clSocket), g_iocp, user_id, 0);

	DWORD flags{};
	memset(&g_clients[user_id].recv_over.over, 0, sizeof(WSAOVERLAPPED));
	int ret = WSARecv(clSocket, g_clients[user_id].recv_over.wsabuf, 1, NULL,
		&flags, &(g_clients[user_id].recv_over.over), NULL);
	if (0 != ret) {
		int err_no = WSAGetLastError();
		if (WSA_IO_PENDING != err_no)
			Server::get()->error_display("WSARecv Error :", err_no);
	}
	//check_login_request(&clSocket, user_id);
	memset(&over_ex->over, 0x00, sizeof(WSAOVERLAPPED));
	SOCKET acceptSock{ WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, NULL, WSA_FLAG_OVERLAPPED) };
	size_t addrLen{ sizeof(SOCKADDR_IN) + 16 };
	over_ex->socket = acceptSock;
	AcceptEx(Server::get()->GetListenSocket(), acceptSock, over_ex->net_buf, NULL, addrLen, addrLen, NULL, &over_ex->over);
}

void WorkerThread::OnEventRecv(const int& key, DWORD& num_byte, OVER_EX* over_ex)
{
	SOCKET client_s{ g_clients[key].socket };
	if (num_byte == 0) {
		DisconnectClient(key, client_s);
		return;
	}

	ProcData(key, over_ex->net_buf, num_byte);
	//ProcessPacket(key, over_ex->net_buf);

	DWORD flags = 0;
	memset(&over_ex->over, 0x00, sizeof(WSAOVERLAPPED));
	WSARecv(client_s, over_ex->wsabuf, 1, 0, &flags, &over_ex->over, 0);
}

void WorkerThread::DisconnectClient(const ULONG& key, const SOCKET& client_s)
{
	{	// 이 블럭을 함수로 빼두고, 접속종료 외에도 일정시간마다 DB에 기록하는 이벤트때 호출하자.
		Knight* leaverObj{ static_cast<Knight*>(g_clients[key].obj) };
		const Coord& curPos{ leaverObj->GetPos() };
		P_INFO saved_player_info{};
		saved_player_info.hp = leaverObj->GetHp();
		saved_player_info.maxHp = leaverObj->GetMaxHp();
		saved_player_info.lvl = leaverObj->GetLevel();
		saved_player_info.atk = leaverObj->GetAtk();
		saved_player_info.curExp = leaverObj->GetCurExp();
		saved_player_info.x = curPos.x;
		saved_player_info.y = curPos.y;
		wmemcpy(saved_player_info.id_str, leaverObj->GetName().c_str(), ID_LEN);
		m_dbMgr.SetUserInfo(saved_player_info);

		leaverObj->SetName(wstring{});
	}

	g_clients[key].is_connected = false;
	closesocket(client_s);

	// 종료한 클라이언트의 뷰 리스트에 존재하는 클라이언트들에게 제거 패킷 전송해야함.
	for (auto& cl : g_clients) {
		if (cl.is_connected == false) continue;
		if (false == Is_NPC(cl.id))
			Server::get()->send_remove_object_packet(cl.id, key);
	}
}



void WorkerThread::ProcData(const int& id, char* buf, DWORD& numByte)
{
	char* ptr = buf;
	size_t in_packet_size{ g_clients[id].curr_packet_size };
	size_t saved_packet_size{ g_clients[id].saved_packet_size };
	char* packet_buffer = g_clients[id].packet_buffer;

	while (0 != numByte) {
		if (0 == in_packet_size) in_packet_size = ptr[0];
		if (numByte + saved_packet_size >= in_packet_size) {
			memcpy(packet_buffer + saved_packet_size, ptr, in_packet_size - saved_packet_size);
			ProcessPacket(id, packet_buffer);
			ptr += in_packet_size - saved_packet_size;
			numByte -= in_packet_size - saved_packet_size;
			in_packet_size = 0;
			saved_packet_size = 0;
		}
		else {
			memcpy(packet_buffer + saved_packet_size, ptr, numByte);
			saved_packet_size += numByte;
			numByte = 0;
		}
	}
	g_clients[id].curr_packet_size = in_packet_size;
	g_clients[id].saved_packet_size = saved_packet_size;
}

void WorkerThread::ProcessPacket(int id, void* buff)
{
	char* packet = reinterpret_cast<char*>(buff);
	// 패킷 타입 => CS_MOVE
	// 패킷 멤버로 DIRECTION 가지고 방향 판단하기.
	switch (packet[1]) {
	case CS_LOGIN:
	{
		cs_packet_login* loginPacket{ reinterpret_cast<cs_packet_login*>(packet) };
		wstring idStr{ loginPacket->name };
		check_login_request(idStr.c_str(), id, loginPacket->is_dummy);
		break;
	}
	case CS_MOVE:
	{
		if (!g_clients[id].obj->IsAlive()) break;
		char mvDir{ packet[2] };
		EVENT ev{ id, high_resolution_clock::now(), EV_MOVE, mvDir };
		Server::get()->add_timer(ev);
		break;
	}
	case CS_ATTACK:
	{
		EVENT ev{ id, high_resolution_clock::now(), EV_ATTACK, 0 };
		Server::get()->add_timer(ev);
		break;
	}
	case CS_TELEPORT:
	{
		cs_packet_teleport* teleportPacket{ reinterpret_cast<cs_packet_teleport*>(packet) };
		Coord dstPos{ teleportPacket->x, teleportPacket->y };

		ProcTeleport(id, dstPos);

		break;
	}
	case CS_HEAL:
	{
		EVENT ev{ id, high_resolution_clock::now() + 1ms, EV_HEAL, 0 };
		Server::get()->add_timer(ev);
		break;
	}
	case CS_CHAT:
		break;

	default: cout << "Invalid Packet Type Error\n"; {
		cout << (int)packet[1] << endl;
		while (true);
	}
	}
}

void WorkerThread::ProcTeleport(const int& id, const Coord& dstPos)
{
	g_clients[id].near_lock.lock_shared();
	auto old_vl = g_clients[id].near_id;
	g_clients[id].near_lock.unlock_shared();

	for (auto& near_id : old_vl) {
		Server::get()->send_remove_object_packet(id, near_id);
		if (!Is_NPC(near_id))
			Server::get()->send_remove_object_packet(near_id, id);
	}

	static_cast<Knight*>(g_clients[id].obj)->TelePort(dstPos);
	// -------------------------
	const auto& mySector{ g_clients[id].mySector };
	const auto& curSector{ GetSector(g_clients[id].obj->GetPos()) };
	MoveSector(id, mySector, curSector);


	unordered_set<int> new_vl{};
	set<pair<short, short> > sectors;
	FindNearSectors(sectors, dstPos);

	for (const auto& pair : sectors) {
		g_sectorlk[pair.first][pair.second].lock_shared();
		const auto sector = g_objectSectors[pair.first][pair.second];
		g_sectorlk[pair.first][pair.second].unlock_shared();
		for (const auto& cl : sector) {
			if (!g_clients[cl].is_connected) continue;
			if (is_near(id, cl)) {
				new_vl.insert(cl);
			}
		}
	}

	//--------------------------------------
	Server::get()->send_teleport_packet(id);

	g_clients[id].near_lock.lock();
	g_clients[id].near_id = new_vl;
	g_clients[id].near_lock.unlock();

	for (const auto& near_id : new_vl) {
		Server::get()->send_put_object_packet(id, near_id);
		if (!Is_NPC(near_id)) Server::get()->send_put_object_packet(near_id, id);
	}
}

void WorkerThread::ProcHeal(const int& id)
{
	g_clients[id].obj->Heal();
}

void WorkerThread::ProcNPCDead(const int& npc_id)
{
	g_clients[npc_id].obj->Die();
	set<pair<short, short> > sectors;
	FindNearSectors(sectors, g_clients[npc_id].obj->GetPos());

	for (const auto& pair : sectors) {
		g_sectorlk[pair.first][pair.second].lock_shared();
		const auto sector = g_objectSectors[pair.first][pair.second];
		g_sectorlk[pair.first][pair.second].unlock_shared();
		for (const auto& cl : sector) {
			if (!g_clients[cl].is_connected) continue;
			if (is_near_npc(npc_id, cl)) {
				if (is_near(npc_id, cl) && !Is_NPC(cl)) Server::get()->sendDeadPacket(cl, npc_id);
			}
		}
	}
}

void WorkerThread::ProcNPCRespawn(const int& key)
{
	NPC* npc = static_cast<NPC*>(g_clients[key].obj);
	npc->Revive();
	const auto& mySector{ g_clients[key].mySector };
	const auto& curSector{ GetSector(npc->GetPos()) };
	MoveSector(key, mySector, curSector);


	set<pair<short, short> > sectors;
	FindNearSectors(sectors, g_clients[key].obj->GetPos());

	for (const auto& pair : sectors) {
		g_sectorlk[pair.first][pair.second].lock_shared();
		const auto sector = g_objectSectors[pair.first][pair.second];
		g_sectorlk[pair.first][pair.second].unlock_shared();
		for (const auto& cl : sector) {
			if (!g_clients[cl].is_connected) continue;
			if (is_near_npc(key, cl)) {
				if (is_near(key, cl) && !Is_NPC(cl)) Server::get()->send_put_object_packet(cl, key);
			}
		}
	}
}

void WorkerThread::do_target_move(int npc_id, int target_id)
{
	// 죽었는지 살았는지 판단하기.
	g_clients[npc_id].near_lock.lock_shared();
	if (false == g_clients[npc_id].is_connected) {
		g_clients[npc_id].near_lock.unlock_shared();
		return;
	}
	g_clients[npc_id].near_lock.unlock_shared();

	// 엔피씨 시야에서 벗어나면 원래 내 위치로 이동해야함.
	if (false == is_near_npc(npc_id, target_id) || !g_clients[target_id].obj->IsAlive()) {
		EVENT ev{ npc_id, high_resolution_clock::now() + 1s, EV_PEACE_NPC, target_id };
		Server::get()->add_timer(ev);
		return;
	}
	SOCKETINFO& npc = g_clients[npc_id];

	set < pair<short, short> >sectors;
	unordered_set <int> old_view_list;
	g_clients[npc_id].near_lock.lock_shared();
	old_view_list = g_clients[npc_id].near_id;
	g_clients[npc_id].near_lock.unlock_shared();

	//FindNearSectors(sectors, npc.obj->GetPos());
	//for (const auto& s : sectors) {
	//	g_sectorlk[s.first][s.second].lock_shared();
	//	const auto sector = g_objectSectors[s.first][s.second];
	//	g_sectorlk[s.first][s.second].unlock_shared();

	//	for (const auto& cl : sector) {
	//		if (!g_clients[cl].is_connected) continue;
	//		if (is_near_npc(npc.id, cl) && !Is_NPC(target_id)) old_view_list.insert(cl); // npc 근처의 pc + npc
	//	}
	//}

	Coord target_pos{ g_clients[target_id].obj->GetPos() };
	static_cast<NPC*>(g_clients[npc_id].obj)->Move(&g_world, target_pos, target_id);
	const auto& prevSector = g_clients[npc_id].mySector;
	const auto& currSector = GetSector(g_clients[npc_id].obj->GetPos());

	MoveSector(npc_id, prevSector, currSector);


	unordered_set <int> new_view_list;
	//sectors.clear();
	FindNearSectors(sectors, npc.obj->GetPos());
	for (const auto& s : sectors) {
		g_sectorlk[s.first][s.second].lock_shared();
		const auto sector = g_objectSectors[s.first][s.second];
		g_sectorlk[s.first][s.second].unlock_shared();
		for (const auto& cl : sector) {
			if (!g_clients[cl].is_connected) continue;
			if (is_near_npc(npc.id, cl) && !Is_NPC(target_id)) new_view_list.insert(cl);
		}
	}

	for (const auto& cl : old_view_list) { // 이동전에 시야에 존재.
		if (0 == new_view_list.count(cl) && !Is_NPC(cl)) { // 이동후에 시야에 없음.
			Server::get()->send_remove_object_packet(cl, npc.id);
		}
		else {
			if (!Is_NPC(cl)) Server::get()->send_move_packet(cl, npc.id);
		}
	}

	for (const auto& cl : new_view_list) {				// 이동 후 시야에 존재.
		if (0 != old_view_list.count(cl) && !Is_NPC(cl)) {	// 이동 전에는 시야에 없었음.
			Server::get()->send_put_object_packet(cl, npc.id);
		}
	}

	g_clients[npc_id].near_lock.lock();
	g_clients[npc_id].near_id = new_view_list;
	g_clients[npc_id].near_lock.unlock();
}

void WorkerThread::do_target_attack(int npc_id, int target_id)
{
	if (g_clients[npc_id].is_connected == false) {
		EVENT ev{ npc_id, high_resolution_clock::now() + 1s, EV_PEACE_NPC, 0 };
	}

	SOCKETINFO& npc{ g_clients[npc_id] };
	SOCKETINFO& target{ g_clients[target_id] };
	npc.obj->Attack(target.obj);

	//	wstring msg = npc.id_str;
	//	msg += L"으로 부터 " + to_wstring(npc.atk) + L"만큼 데미지를 입었습니다.";
	//	Server::get()->send_chat_packet(target_id, SYSTEM_CHAT, msg);

}

void WorkerThread::ProcPCDead(int deadman, int npc_id)
{
	g_clients[deadman].obj->Die();
	g_clients[deadman].near_lock.lock_shared();
	const auto& nearIds = g_clients[deadman].near_id;
	g_clients[deadman].near_lock.unlock_shared();

	Server::get()->sendDeadPacket(deadman, deadman);
	for (const auto& cl : nearIds) {
		if (!Is_NPC(cl)) Server::get()->sendDeadPacket(cl, deadman);
	}
}

bool WorkerThread::is_near_id(int player, int other)
{
	g_clients[player].near_lock.lock_shared();
	const auto vl = g_clients[player].near_id;
	g_clients[player].near_lock.unlock_shared();
	return 0 != vl.count(other);
}

void WorkerThread::MovePlayer(int id, char dir)
{
	// 실제 이동( 장애물, 맵의 끝 확인, 좌표 이동)
	g_clients[id].obj->Move(g_world, dir);

	const auto& prevSector = g_clients[id].mySector;
	const auto& currSector = GetSector(g_clients[id].obj->GetPos());

	MoveSector(id, prevSector, currSector);

	// 이동한 플레이어의 뷰 리스트(기존, 이동후의 뷰 리스트)를 비교하여 시야처리(시야에 들어온/나간 객체 표시)
	g_clients[id].near_lock.lock_shared();
	const auto& old_vl = g_clients[id].near_id; 
	g_clients[id].near_lock.unlock_shared();

	Server::get()->send_move_packet(id, id);
	set<pair<short, short>> sectors{};
	FindNearSectors(sectors, g_clients[id].obj->GetPos());

	unordered_set <int> new_vl;
	for (const auto& s : sectors) {
		g_sectorlk[s.first][s.second].lock_shared();
		const auto sector = g_objectSectors[s.first][s.second];
		g_sectorlk[s.first][s.second].unlock_shared();
		for (const auto& cl : sector) {
			if (!g_clients[cl].is_connected) continue;
			if (is_near_npc(id, cl)) {
				if (Is_NPC(cl) && !g_clients[cl].obj->IsActive()) {
					EVENT ev{ cl, high_resolution_clock::now() + 1s, EV_PLAYER_MOVE_NOTIFY, id };
					Server::get()->add_timer(ev);
				}
				if (is_near(id, cl)) new_vl.insert(cl);
			}
		}
	}

	for (const auto& cl : old_vl) {
		if (0 != new_vl.count(cl)) {
			if (!Is_NPC(cl)) Server::get()->send_move_packet(cl, id);
		}
		else {
			Server::get()->send_remove_object_packet(id, cl);
			if (!Is_NPC(cl)) Server::get()->send_remove_object_packet(cl, id);
		}
	}
	for (const auto& cl : new_vl) {
		if (0 == old_vl.count(cl)) {
			Server::get()->send_put_object_packet(id, cl);
			if (false == Is_NPC(cl)) Server::get()->send_put_object_packet(cl, id);
		}
	}
}

void WorkerThread::OnEventPCAttack(OVER_EX* over_ex, const int& id)
{
	// 대상 찾기	
	g_clients[id].near_lock.lock_shared();
	auto old_vl = g_clients[id].near_id;
	g_clients[id].near_lock.unlock_shared();

	Object* target{};
	for (auto& targetId : old_vl) {
		const Coord& t = g_clients[targetId].obj->GetPos();
		if (g_clients[id].obj->IsInRange(t) && g_clients[id].obj->IsAlive()) {
			target = g_clients[targetId].obj;
			g_clients[id].obj->Attack(target);
		}
	}

	delete over_ex;

	/*
	//cout << "플레이어 " << id << "가 공격을 했습니다." << endl;
	//short x, y;
	//x = g_clients[id].x;
	//y = g_clients[id].y;

	//g_clients[id].near_lock.lock();
	//auto vl = g_clients[id].near_id;
	//g_clients[id].near_lock.unlock();

	//int atk = g_clients[id].atk;
	//bool is_inRange = false;
	//for (auto& obj : vl) {
	//	auto& target = g_clients[obj];

	//	if (target.x == (x - 1) && target.y == y) is_inRange = true; // 공격자 위치 왼쪽
	//	else if (target.x == (x + 1) && target.y == y) is_inRange = true;
	//	else if (target.x == x && target.y == (y - 1)) is_inRange = true;
	//	else if (target.x == x && target.y == (y + 1)) is_inRange = true;
	//	else is_inRange = false;

	//	if (is_inRange == true) {
	//		target.hp -= atk;
	//		if (target.type == PALADIN) {
	//			EVENT ev{ target.id, high_resolution_clock::now() + 1s, EV_ATTACKED_MONSTER, id };
	//			Server::get()->add_timer(ev);
	//		}
	//		wstring msg;
	//		msg += to_wstring(id) + L"가 " + to_wstring(obj) + L"에게 "
	//			+ to_wstring(g_clients[id].atk) + L"만큼 피해를 입혔습니다.";

	//		Server::get()->send_chat_packet(id, SYSTEM_CHAT, msg);
	//	}

	//	if (target.hp <= 0)
	//	{
	//		// 매 공격마다 검사?
	//		// 경험치 획득, 레벨업, 스탯 체인지 처리
	//		g_clients[id].exp += target.exp;
	//		wstring msg = target.id_str;
	//		msg += L"를 잡아 경험치 " + to_wstring(target.exp) + L"를 얻었습니다.";
	//		Server::get()->send_chat_packet(id, SYSTEM_CHAT, msg);
	//		auto& attacker = g_clients[id];
	//		int goal_exp = 100 * pow(2, attacker.lvl - 1);
	//		if (goal_exp <= attacker.exp) {

	//			msg = L"레벨이 상승했습니다. (LV." + to_wstring(attacker.lvl) + L" => LV," + to_wstring(attacker.lvl + 1) + L")";
	//			Server::get()->send_chat_packet(id, SYSTEM_CHAT, msg);

	//			//cout << attacker->id << " 레벨업\n";
	//			//cout << attacker->lvl;
	//			attacker.lvl += 1;
	//			//cout << " ==> " << attacker->lvl << endl;
	//			//cout << "EXP " << attacker->exp << " / " << goal_exp << endl;
	//			attacker.exp = attacker.exp - goal_exp;
	//			goal_exp = 100 * pow(2, attacker.lvl - 1);
	//			//cout << "==> EXP " << attacker->exp << " / " << goal_exp << endl;
	//			//cout << "ATK " << attacker->atk;
	//			attacker.atk += attacker.atk * 0.2 + 5;
	//			short max_hp = 100 + (pow(attacker.lvl, 2) * 10);
	//			attacker.info.hp = max_hp; // 죽었을때 풀체력으로 돌리기 위해 여기에 저장.
	//			attacker.hp = max_hp;
	//			//cout << " ==> " << attacker->atk;
	//			Server::get()->send_stat_change_packet(id);
	//		}

	//		for (int i = 0; i < NPC_ID_START; ++i) {
	//			if (false == g_clients[i].is_connected) continue;
	//			if (is_near(i, target.id)) {
	//				Server::get()->send_remove_object_packet(i, target.id);
	//				target.is_connected = false;
	//				OVER_EX* over_ex = new OVER_EX;
	//				*(int*)over_ex->net_buf = id;
	//				over_ex->ev_type = EV_DIE_MONSTER;
	//				PostQueuedCompletionStatus(g_iocp, 1, target.id, &over_ex->over);
	//			}
	//		}
	//	}
	//}
	*/
}

void WorkerThread::check_login_request(const wchar_t* id_str, int id, const bool& is_dummy)
{
	// DB에 쿼리해서 로그인 요청이 VAILD 또는 INVAILD 인지 체크하는 함수
	short row{ -1 };
	short col{ -1 };
	Coord pos{};
	if (!is_dummy) {
		// 중복 ID 검사
		for (int i = 0; i < NPC_ID_START; ++i) {
			if (false == g_clients[i].is_connected) continue;
			if (id_str == g_clients[i].obj->GetName()) {
				cout << "로그인 실패" << endl;
				Server::get()->send_login_fail_packet(id);
				return;
			}
		}

		// 여기서 처리하지 말고 PQCS로 넘겨?
		P_INFO playerInfo = m_dbMgr.GetUserInfo(id_str);

		if (playerInfo.lvl == NULL)
			playerInfo = m_dbMgr.AddUserInfo(id_str);

		pos.x = playerInfo.x;
		pos.y = playerInfo.y;

		g_clients[id].near_lock.lock();
		*g_clients[id].obj = Knight(id, playerInfo.id_str,
			KNIGHT,
			playerInfo.hp,
			playerInfo.maxHp,
			playerInfo.lvl,
			playerInfo.curExp,
			MAX_EXP_PER_LEVEL[playerInfo.lvl],
			playerInfo.atk,
			pos
		);

		row = g_clients[id].mySector.first = playerInfo.x / SECTOR_WIDTH;
		col = g_clients[id].mySector.second = playerInfo.y / SECTOR_HEIGHT;
		g_clients[id].near_lock.unlock();

		// 3분 주기로 DB에 플레이어 위치 갱신.
		EVENT ev{ id, high_resolution_clock::now() + 3min, EV_SAVE_PLAYER_INFO, 0 };
		Server::get()->add_timer(ev);
	}
	else {
		pos = { rand() % 800, rand() % 800 };
		g_clients[id].near_lock.lock();
		*g_clients[id].obj = Knight{ id, id_str,
			DUMMY_KNIGHT,
			65500,
			65500,
			60,
			0,
			MAX_EXP_PER_LEVEL[60],
			100,
			pos
		};
		row = g_clients[id].mySector.first = pos.x / SECTOR_WIDTH;
		col = g_clients[id].mySector.second = pos.y / SECTOR_HEIGHT;

		g_clients[id].near_lock.unlock();
	}

	g_sectorlk[row][col].lock();
	g_objectSectors[row][col].insert(id);
	g_sectorlk[row][col].unlock();
	Server::get()->send_login_ok_packet(id);

	set<pair<short, short>> sectors;
	FindNearSectors(sectors, pos);
	// 섹터에 넣어줘야한다.
	// 인접 섹터를 찾아서 반복해줘야한다.


	EVENT ev{ -1, high_resolution_clock::now() + 1s, EV_PLAYER_MOVE_NOTIFY, id };
	for (const auto& s : sectors) {
		g_sectorlk[row][col].lock_shared();
		const auto sector = g_objectSectors[s.first][s.second];
		g_sectorlk[row][col].unlock_shared();
		for (const auto& cl : sector) {
			if (is_near(id, cl)) {
				Server::get()->send_put_object_packet(id, cl);
				if (false == Is_NPC(cl)) Server::get()->send_put_object_packet(cl, id);
			}

			if (Is_NPC(cl) && !g_clients[cl].obj->IsActive() && is_near_npc(cl, id)) {
				ev.obj_id = cl;
				Server::get()->add_timer(ev);
			}
		}
	}


	//for (const auto& cl : g_clients) {
	//	int other_player = cl.id;
	//	if (false == g_clients[other_player].is_connected) continue;
	//	if (true == is_near(other_player, id)) {
	//		Server::get()->send_put_object_packet(id, other_player); // 현재 입장한 플레이어에게 다른 플레이어 알려주기.
	//		if (false == Is_NPC(other_player))
	//			Server::get()->send_put_object_packet(other_player, id); // 다른 플레이어에게 현재 입장한 플레이어 알려주기.
	//	}
	//	if (true == Is_NPC(other_player) &&
	//		false == cl.obj->IsActive() &&
	//		true == is_near_npc(other_player, id)) {
	//		// ai 깨워주기 내가 깨웠는데, 다른 플레이어가 또 깨움 ==> 이동속도가 두배가 됨. 예외처리 필요1
	//		// 이미 이동하고 있는지 확인한다.
	//		EVENT ev{ other_player, high_resolution_clock::now() + 1s, EV_PLAYER_MOVE_NOTIFY, id };
	//		Server::get()->add_timer(ev);
	//	}
	//}

}


bool WorkerThread::Is_NPC(int id)
{
	return id >= NPC_ID_START;
}

bool WorkerThread::is_near(int a, int b)
{
	Coord curPosA{ g_clients[a].obj->GetPos() };
	Coord curPosB{ g_clients[b].obj->GetPos() };

	if (VIEW_RANGE < abs(curPosA.x - curPosB.x)) return false;
	if (VIEW_RANGE < abs(curPosA.y - curPosB.y)) return false;
	return true;
}

bool WorkerThread::is_near_npc(int a, int b)
{
	Coord curPosA{ g_clients[a].obj->GetPos() };
	Coord curPosB{ g_clients[b].obj->GetPos() };

	if (NPC_VIEW_RANGE < abs(curPosA.x - curPosB.x)) return false;
	if (NPC_VIEW_RANGE < abs(curPosA.y - curPosB.y)) return false;
	return true;
}

void WorkerThread::FindNearSectors(set<pair<short, short>>& sectors, const Coord& pos)
{
	// 인접 섹터 구하기
	// 인접한 섹터?
	// LeftTop.x, LeftTop.y 가 포함된 섹터
	// RightBottom.x, RightBottom.y가 포함된 섹터

	// 시야 경계에 해당하는 위치
	Coord leftTop{ pos.x - VIEW_RANGE, pos.y - VIEW_RANGE };
	Coord rightBottom{ pos.x + VIEW_RANGE, pos.y + VIEW_RANGE };

	// 시야 경계로 구한 섹터 행, 열 번호 (중복될 수 있다. set은 중복 허용을 하지 않음)
	short leftRow{ (short)(leftTop.x / SECTOR_WIDTH) };
	short topCol{ (short)(leftTop.y / SECTOR_HEIGHT) };
	short rightRow{ (short)(rightBottom.x / SECTOR_WIDTH) };
	short bottomCol{ (short)(rightBottom.y / SECTOR_HEIGHT) };

	// 시야가 맵 범위를 벗어난 경우에는 삽입하지 않음.
	if (leftTop.x >= 0) {
		if (leftTop.y >= 0)	sectors.insert(make_pair(leftRow, topCol));
		if (rightBottom.y < WORLD_HEIGHT) sectors.insert(make_pair(leftRow, bottomCol));
	}
	if (rightBottom.x < WORLD_WIDTH) {
		if (rightBottom.y < WORLD_HEIGHT) sectors.insert(make_pair(rightRow, bottomCol));
		if (leftTop.y >= 0) sectors.insert(make_pair(rightRow, topCol));
	}
}

pair<short, short> WorkerThread::GetSector(const Coord& pos)
{
	short row = pos.x / SECTOR_WIDTH;
	short col = pos.y / SECTOR_HEIGHT;
	return make_pair(row, col);
}

void WorkerThread::MoveSector(const int& id, const pair<short, short>& mySector, const pair<short, short>& curSector)
{
	if (mySector != curSector) {
		g_sectorlk[mySector.first][mySector.second].lock();
		g_objectSectors[mySector.first][mySector.second].erase(id);
		g_sectorlk[mySector.first][mySector.second].unlock();

		g_sectorlk[curSector.first][curSector.second].lock();
		g_objectSectors[curSector.first][curSector.second].insert(id);
		g_sectorlk[curSector.first][curSector.second].unlock();

		g_clients[id].mySector = curSector;
	}
}