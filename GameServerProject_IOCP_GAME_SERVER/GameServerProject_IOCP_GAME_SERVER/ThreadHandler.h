#pragma once
#include <vector>
#include "MyThread.h"
class ThreadHandler
{
public:
	ThreadHandler();
	~ThreadHandler();

public:
	void CreateThread();
	void JoinThreads();

private:
	void AddThread(MyThread* thread);
	std::vector<MyThread*> m_myThreads;
};

