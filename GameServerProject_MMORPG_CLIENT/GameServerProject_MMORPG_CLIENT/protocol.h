#pragma once
#pragma once

constexpr int MAX_STR_LEN = 50;

#define WORLD_WIDTH		800
#define WORLD_HEIGHT	800

#define NPC_ID_START	100	// 0 ~ 99 pc / 100 ~ npc
#define NUM_NPC			50000

#define SERVER_PORT		3500

#define SINGLE_ID		1

//#define CS_UP			1
//#define CS_DOWN			2
//#define CS_LEFT			3
//#define CS_RIGHT		4
#define CS_MOVE			1
#define CS_ATTACK		2
#define CS_LOGIN		5

//===========================
#define DIR_UP			1
#define DIR_DOWN		2
#define DIR_LEFT		3
#define DIR_RIGHT		4
//===========================

#define SC_LOGIN_OK			1
#define SC_LOGIN_FAIL		2
#define SC_PUT_PLAYER		3
#define SC_REMOVE_PLAYER	4
#define SC_POS				5
#define SC_CHAT				6

#pragma pack(push ,1)

struct sc_packet_pos {
	char size;
	char type;
	int id;
	char x, y;
};

struct sc_packet_remove_player {
	char size;
	char type;
	int id;
};

struct sc_packet_login_ok {
	char size;
	char type;
	int id;
	short x, y;
	/*
	short hp
	short level;
	short exp;
	*/
};

struct sc_packet_login_fail{
	char size;
	char type;
};

struct sc_packet_put_player {
	char size;
	char type;
	int id;
	short x, y;
};

struct sc_packet_chat{
	char size;
	char type;
	int id;
	char chat[MAX_STR_LEN];
};

struct cs_packet_move{
	char size;
	char type;
	char dir;
};

struct cs_packet_login_request{
	char	size;
	char	type;
	char	ID_STR[20];
};

struct cs_packet_attack{
	char size;
	char type;
};

#pragma pack (pop)