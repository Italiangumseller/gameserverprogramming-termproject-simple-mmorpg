#define SFML_STATIC 1
#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>
#include <iostream>
#include <unordered_map>
#include <chrono>
#include <fstream>
using namespace std;
using namespace chrono;

#ifdef _DEBUG
#pragma comment (lib, "lib/sfml-graphics-s-d.lib")
#pragma comment (lib, "lib/sfml-window-s-d.lib")
#pragma comment (lib, "lib/sfml-system-s-d.lib")
#pragma comment (lib, "lib/sfml-network-s-d.lib")
#else
#pragma comment (lib, "lib/sfml-graphics-s.lib")
#pragma comment (lib, "lib/sfml-window-s.lib")
#pragma comment (lib, "lib/sfml-system-s.lib")
#pragma comment (lib, "lib/sfml-network-s.lib")
#endif
#pragma comment (lib, "freetype.lib")
#pragma comment (lib, "opengl32.lib")
#pragma comment (lib, "winmm.lib")
#pragma comment (lib, "ws2_32.lib")

#include "../../GameServerProject_IOCP_GAME_SERVER/GameServerProject_IOCP_GAME_SERVER/protocol.h"
sf::TcpSocket socket;

constexpr auto SCREEN_WIDTH = 20;
constexpr auto SCREEN_HEIGHT = 14;

constexpr auto TILE_WIDTH = 65;
constexpr auto WINDOW_WIDTH = TILE_WIDTH * SCREEN_WIDTH + 10;   // size of window
constexpr auto WINDOW_HEIGHT = TILE_WIDTH * SCREEN_HEIGHT + 10;
constexpr auto BUF_SIZE = 200;
constexpr auto MAX_USER = 20000;

int g_left_x;
int g_top_y;
int g_myid;

sf::RenderWindow* g_window;
sf::Font g_font;


int g_world[WORLD_WIDTH][WORLD_HEIGHT];

static high_resolution_clock::time_point g_last_move_time{ high_resolution_clock::now() };
static high_resolution_clock::time_point g_last_attack_time{ high_resolution_clock::now() };

enum PlayerAnimate
{
	IDLE = 3, WALK = 7, ATTACK = 9	// sprite 개수와 상태
};

class OBJECT {
private:
	bool m_showing;
	sf::Sprite m_sprite;
	char m_msg[MAX_STR_LEN];
	high_resolution_clock::time_point m_time_out;
	sf::Text m_text;
	int m_state{ IDLE };
	int anim_frame{ 0 };

private:
	short m_hp;
	short m_level;
	short m_exp;
	short m_max_exp;
	char m_type;

public:
	bool m_healing{ true };
	int m_x, m_y;
	OBJECT(sf::Texture& t, int x, int y, int x2, int y2) {
		m_showing = false;
		m_sprite.setTexture(t);
		m_sprite.setTextureRect(sf::IntRect(x, y, x2, y2));
	}
	OBJECT() {
		m_showing = false;
	}

	void setState(int state) {
		anim_frame = 0;
		m_state = state;
	}
	int getState() {
		return m_state;
	}

	void setHp(short hp) {
		m_hp = hp;
	}
	short getHp() {
		return m_hp;
	}

	void setLevel(short lv) {
		m_level = lv;
	}
	short getLevel() {
		return m_level;
	}

	void setExp(short exp) {
		m_exp = exp;
	}
	short getExp() {
		return m_exp;
	}

	void setMaxExp(short max_ex) {
		m_max_exp = max_ex;
	}
	short getMaxExp() {
		return m_max_exp;
	}

	void setType(char type) {
		m_type = type;
	}
	char getType() {
		return m_type;
	}

	void show()
	{
		m_showing = true;
	}
	void hide()
	{
		m_showing = false;
	}

	void animate(int state) {
		if (state == IDLE) {
			int width = 63.f, height = 63.f;
			m_sprite.setTextureRect(sf::IntRect(anim_frame++ * width, height * 0, width, height));
			if (anim_frame == IDLE)
				anim_frame = 0;
		}

		else if (state == WALK) {
			int width = 63.f, height = 63.f;
			m_sprite.setTextureRect(sf::IntRect(anim_frame++ * width, height * 1, width, height));
			if (anim_frame == WALK) {
				anim_frame = 0;
				m_state = IDLE;
			}
			m_healing = false;
		}

		else if (state == ATTACK) {
			int width = 120.f, height = 63.f;
			m_sprite.setTextureRect(sf::IntRect(anim_frame++ * width, height * 2, width, height));
			if (anim_frame == ATTACK) {
				anim_frame = 0;
				m_state = IDLE;
			}
			m_healing = false;
		}
	}

	void a_move(int x, int y) {
		m_sprite.setPosition((float)x, (float)y);
	}

	void a_draw() {
		g_window->draw(m_sprite);
	}

	void move(int x, int y) {
		m_x = x;
		m_y = y;
	}

	void draw() {
		if (false == m_showing) return;
		char obj_type = getType();
		float rx;
		float ry;
		if (obj_type == MY_CHARACTER) {
			rx = (m_x - g_left_x) * 65.0f + 8;
			ry = (m_y - g_top_y) * 65.0f - 16;
		}
		else {
			rx = (m_x - g_left_x) * 65.0f + 30.f;
			ry = (m_y - g_top_y) * 65.f - 14.f;
		}
		m_sprite.setPosition(rx, ry);
		g_window->draw(m_sprite);
		if (high_resolution_clock::now() < m_time_out) {
			m_text.setPosition(rx - 10, ry - 10);
			g_window->draw(m_text);
		}
		//cout << rx << ", " << ry << endl;
	}

	void add_chat(wchar_t chat[]) {
		m_text.setFont(g_font);
		m_text.setString(chat);
		m_time_out = high_resolution_clock::now() + 1s;
	}
};

OBJECT avatar;
OBJECT players[MAX_USER];
unordered_map <int, OBJECT> npcs;
OBJECT chat_ui;

OBJECT tiles[5];


sf::Texture* other_knights;
sf::Texture* maps;
sf::Texture* knight;
sf::Texture* zombie;
sf::Texture* rogue;
sf::Texture* slayer;
sf::Texture* darkwarrior;
sf::Texture* paladin;

void client_initialize()
{
	other_knights = new sf::Texture;
	maps = new sf::Texture;
	knight = new sf::Texture;
	zombie = new sf::Texture;
	rogue = new sf::Texture;
	slayer = new sf::Texture;
	darkwarrior = new sf::Texture;
	paladin = new sf::Texture;

	//chat_ui.move( 755, 460 );
	if (false == g_font.loadFromFile("cour.ttf")) {
		cout << "Font Loading Error!\n";
		while (true);
	}
	other_knights->loadFromFile("Texture/knight_sprite_sheet.png");
	maps->loadFromFile("Texture/TileMap.png");
	knight->loadFromFile("Texture/knight_sprite_sheet.png");
	zombie->loadFromFile("Texture/zombie.png");;
	rogue->loadFromFile("Texture/Rogue.png");;
	slayer->loadFromFile("Texture/Slayer.png");;
	darkwarrior->loadFromFile("Texture/DarkWarrior.png");;
	paladin->loadFromFile("Texture/Paladin.png");;

	//for (int i = 0 ; i < 4 ; ++i )
	avatar = OBJECT{ *knight, 0, 0, 63, 63 };
	//avatar.a_move( 4, 4 );

	for (auto& pl : players) {
		pl = OBJECT{ *other_knights, 64, 0, 64, 64 };
	}

	for (int i = 0; i < 5; ++i) {
		tiles[i] = OBJECT(*maps, 64 * i, 0, TILE_WIDTH, TILE_WIDTH);
	}
	int tile{ -1 };

	ifstream in("GameWorld.txt");

	for (int i = 0; i < WORLD_HEIGHT; ++i) {
		for (int j = 0; j < WORLD_WIDTH; ++j)
		{
			in >> tile;
			g_world[i][j] = tile;
		}
	}
	//ofstream out{ "GameWorld.txt", ios::binary};
	////out.open( "GameWorld.txt" );
	//cout << "맵을 파일로 만드는 중\n";
	//for( int i = 0; i < WORLD_HEIGHT; ++i ){
	//	for( int j = 0; j < WORLD_WIDTH; ++j ){
	//		out << g_world[i][j];
	//		out << ' '; 
	//	}
	//	out << endl;
	//}

	//out.close();
	//cout << "맵 다 만듬!\n";
}

void client_finish()
{
	delete other_knights;
	delete maps;
	delete knight;
	delete zombie;
	delete rogue;
	delete slayer;
	delete darkwarrior;
	delete paladin;
}

void ProcessPacket(char* ptr)
{
	static bool first_time = true;
	switch (ptr[1])
	{
	case SC_LOGIN_OK:
	{
		// hp, level. exp 받아야함.
		sc_packet_login_ok* packet = reinterpret_cast<sc_packet_login_ok*>(ptr);
		g_myid = packet->id;
		avatar.setLevel(packet->level);
		avatar.setExp(packet->exp);
		avatar.setHp(packet->hp);
		avatar.setMaxExp(100 * pow(2.f, (packet->level - 1)));
		avatar.show();
		break;
	}
	case SC_LOGIN_FAIL:
	{
		cout << "이미 접속한 플레이어의 아이디로 접속 요청을 했습니다." << endl;
		socket.disconnect();
		exit(1);
	}
	case SC_PUT_OBJECT:
	{
		sc_packet_put_object* my_packet = reinterpret_cast<sc_packet_put_object*>(ptr);
		int id = my_packet->id;
		// 타입을 확인해야한다. (몬스터 타입 구분!)
		if (id == g_myid) {
			avatar.move(my_packet->x, my_packet->y);
			g_left_x = my_packet->x - 9;
			g_top_y = my_packet->y - 7;
			avatar.show();
			avatar.setType(MY_CHARACTER);
		}
		else if (id < MAX_USER) {
			players[id].move(my_packet->x, my_packet->y);
			players[id].show();
		}
		else {
			switch (my_packet->obj_type)
			{
			case ZOMBIE:
				npcs[id] = OBJECT{ *zombie, 0, 0, 64, 64 };
				npcs[id].setType(ZOMBIE);
				break;
			case ROGUE:
				npcs[id] = OBJECT{ *rogue, 0, 0, 31, 50 };
				npcs[id].setType(ROGUE);
				break;
			case SLAYER:
				npcs[id] = OBJECT{ *slayer, 0, 0, 34, 60 };
				npcs[id].setType(SLAYER);
				break;
			case DARKWARRIOR:
				npcs[id] = OBJECT{ *darkwarrior, 0, 0, 37, 60 };
				npcs[id].setType(DARKWARRIOR);
				break;
			case PALADIN:
				npcs[id] = OBJECT{ *paladin, 0, 0, 21, 53 };
				npcs[id].setType(PALADIN);
				break;
			}
			npcs[id].move(my_packet->x, my_packet->y);
			npcs[id].show();
		}
		break;
	}
	case SC_MOVE:
	{
		sc_packet_move* my_packet = reinterpret_cast<sc_packet_move*>(ptr);
		int other_id = my_packet->id;
		if (other_id == g_myid) {
			avatar.move(my_packet->x, my_packet->y);
			g_left_x = my_packet->x - 9;
			g_top_y = my_packet->y - 7;
		}
		else if (other_id < MAX_USER) {
			players[other_id].move(my_packet->x, my_packet->y);
		}
		else {
			if (0 != npcs.count(other_id))
				npcs[other_id].move(my_packet->x, my_packet->y);
		}
		break;
	}

	case SC_REMOVE_OBJECT:
	{
		sc_packet_remove_object* my_packet = reinterpret_cast<sc_packet_remove_object*>(ptr);
		int other_id = my_packet->id;
		if (other_id == g_myid) {
			avatar.hide();
		}
		else if (other_id < MAX_USER) {
			players[other_id].hide();
		}
		else {
			npcs[other_id].hide();
		}
		break;
	}
	case SC_CHAT:
	{
		sc_packet_chat* my_packet = reinterpret_cast<sc_packet_chat*>(ptr);
		int other_id = my_packet->id;
		if (other_id == SYSTEM_CHAT) {
			wcout << my_packet->chat << endl;
			chat_ui.add_chat(my_packet->chat);
		}
		else if (other_id == g_myid) {
			avatar.add_chat(my_packet->chat);
		}
		else if (other_id < MAX_USER) {
			players[other_id].add_chat(my_packet->chat);
		}
		else {
			if (0 != npcs.count(other_id))
				npcs[other_id].add_chat(my_packet->chat);
		}
		break;
	}
	case SC_STAT_CHANGE:
	{
		// 레벨 업
		sc_packet_stat_change* packet = reinterpret_cast<sc_packet_stat_change*>(ptr);
		avatar.setLevel(packet->level);
		avatar.setExp(packet->exp);
		avatar.setHp(packet->hp);
		avatar.setMaxExp(100 * pow(2.f, (packet->level - 1)));
		break;
	}
	case SC_TELEPORT:
	{
		sc_packet_teleport* packet{ reinterpret_cast<sc_packet_teleport*>(ptr) };
		avatar.move(packet->x, packet->y);
		g_left_x = packet->x - 9;
		g_top_y = packet->y - 7;
		avatar.show();
		break;
	}
	case SC_DEAD:
	{
		sc_packet_dead* packet{ reinterpret_cast<sc_packet_dead*>(ptr) };
		int deadman{ packet->id };
		if (deadman == g_myid) avatar.hide();
		else if (deadman < MAX_USER) players[deadman].hide();
		else npcs[deadman].hide();
		break;
	}
	default:
		printf("Unknown PACKET type [%d]\n", ptr[1]);
	}

}

void process_data(char* net_buf, size_t io_byte)
{
	char* ptr = net_buf;
	static unsigned char in_packet_size = 0;
	static size_t saved_packet_size = 0;
	static char packet_buffer[BUF_SIZE];

	while (0 != io_byte) {
		if (0 == in_packet_size) in_packet_size = ptr[0];
		if (io_byte + saved_packet_size >= in_packet_size) {
			memcpy(packet_buffer + saved_packet_size, ptr, in_packet_size - saved_packet_size);
			ProcessPacket(packet_buffer);
			ptr += in_packet_size - saved_packet_size;
			io_byte -= in_packet_size - saved_packet_size;
			in_packet_size = 0;
			saved_packet_size = 0;
		}
		else {
			memcpy(packet_buffer + saved_packet_size, ptr, io_byte);
			saved_packet_size += io_byte;
			io_byte = 0;
		}
	}
}

void client_main()
{
	char net_buf[BUF_SIZE];
	size_t	received;

	auto recv_result = socket.receive(net_buf, BUF_SIZE, received);
	if (recv_result == sf::Socket::Error)
	{
		wcout << L"Recv 에러!";
		while (true);
	}
	if (recv_result != sf::Socket::NotReady)
		if (received > 0) process_data(net_buf, received);

	for (int i = 0; i < SCREEN_WIDTH; ++i)
		for (int j = 0; j < SCREEN_HEIGHT; ++j)
		{
			int tile_x = i + g_left_x;
			int tile_y = j + g_top_y;
			if ((tile_x < 0) || (tile_y < 0)) continue;
			if (tile_x >= WORLD_WIDTH || tile_y >= WORLD_HEIGHT) continue;
			tiles[g_world[tile_x][tile_y]].a_move(TILE_WIDTH * i + 7, TILE_WIDTH * j + 7);
			tiles[g_world[tile_x][tile_y]].a_draw();
		}

	int state = avatar.getState();
	avatar.animate(state);
	avatar.draw();



	for (auto& pl : players) pl.draw();
	for (auto& npc : npcs) npc.second.draw();
	//chat_ui.draw();
	sf::Text text;
	text.setFont(g_font);
	char buf[100];
	sprintf_s(buf, "(%d, %d)", avatar.m_x, avatar.m_y);
	text.setString(buf);
	g_window->draw(text);

}

void send_move_packet(int dir)
{
	cs_packet_move packet;
	packet.size = sizeof(packet);
	packet.type = CS_MOVE;
	packet.direction = dir;
	size_t sent = 0;
	socket.send(&packet, sizeof(packet), sent);
}

void send_login_request_packet(const wstring& user_id)
{
	cs_packet_login packet;
	packet.size = sizeof(packet);
	packet.type = CS_LOGIN;
	packet.is_dummy = 0;
	wmemcpy(packet.name, user_id.c_str(), user_id.length());

	size_t sent = 0;
	socket.send(&packet, sizeof(packet), sent);
}

void send_attack_packet() {
	cs_packet_attack packet;
	packet.size = sizeof(packet);
	packet.type = CS_ATTACK;
	size_t sent = 0;
	socket.send(&packet, packet.size, sent);
}

void send_teleport_packet(short x, short y) {
	cs_packet_teleport packet;
	packet.size = sizeof(packet);
	packet.type = CS_TELEPORT;
	packet.x = x;
	packet.y = y;
	size_t sent{};
	socket.send(&packet, packet.size, sent);
}

void sendHealPacket() {
	cs_packet_heal packet;
	packet.size = sizeof(packet);
	packet.type = CS_HEAL;
	size_t sent{};
	socket.send(&packet, packet.size, sent);
}

int main()
{

	wcout.imbue(locale("korean"));
	sf::Socket::Status status = socket.connect("127.0.0.1", SERVER_PORT);
	socket.setBlocking(false);

	if (status != sf::Socket::Done) {
		wcout << L"서버와 연결할 수 없습니다.\n";
		while (true);
	}

	cout << "Input User Id: ";
	wstring user_id;
	wcin >> user_id;
	user_id += L'\0';

	send_login_request_packet(user_id);

	client_initialize();

	sf::RenderWindow window(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), "2D CLIENT");
	g_window = &window;

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
			if (event.type == sf::Event::KeyPressed) {
				int dir = -1;
				switch (event.key.code) {
				case sf::Keyboard::Left:
					dir = DIR_LEFT;
					avatar.setState(WALK);
					break;
				case sf::Keyboard::Right:
					dir = DIR_RIGHT;
					avatar.setState(WALK);
					break;
				case sf::Keyboard::Up:
					dir = DIR_UP;
					avatar.setState(WALK);
					break;
				case sf::Keyboard::Down:
					dir = DIR_DOWN;
					avatar.setState(WALK);
					break;
				case sf::Keyboard::A:
				{
					auto atkTime{ high_resolution_clock::now() };
					if (atkTime - g_last_attack_time >= 1000ms) {
						send_attack_packet();
						g_last_attack_time = atkTime;
						avatar.setState(ATTACK);
					}
					break;
				}
				case sf::Keyboard::H:
				{
					sendHealPacket();
					break;
				}
				case sf::Keyboard::Escape:
					window.close();
					break;
				case sf::Keyboard::Space: {
					cout << "input teleport coord: ";
					short x{}, y{};
					cin >> x >> y;
					send_teleport_packet(x, y);
					break;
				}
				}
				if (-1 != dir) {
					auto mvTime{ high_resolution_clock::now() };
					if (mvTime - g_last_move_time >= 100ms) {
						send_move_packet(dir);
						g_last_move_time = mvTime;
					}
				}
			}
		}


		window.clear();
		client_main();
		window.setFramerateLimit(10);
		window.display();
	}
	client_finish();

	return 0;
}